<?php
	$title = "MyHotels - Kuukausiraportti";
	require("config.php");
	
	if ($_SESSION['role'] == 4) {
		
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = NULL;
		}
		
		switch($action) {
			case 'get':
				$data = $reports->monthlySales($_POST['year'], $_POST['month']);
				break;
			default:
				$data = NULL;
				break;
		
		}

		require("Views/Reports/MonthlyReport.view.php");
	
	} else {
		header("Location: index.php");
	}
?>