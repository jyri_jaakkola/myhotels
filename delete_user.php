<?php
	$title = "MyHotels - Poista asiakas";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 1) {
		$id = 0;
		$action = NULL;
		
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
		}
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		
		switch($action) {
			case 'customer':
				$user->deleteUser($id, false);
				header("Location: dashboard.php");
				break;
			case 'employee':
				$user->deleteUser($id, true);
				header("Location: dashboard.php");
				break;
			default:
				header("Location: index.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>