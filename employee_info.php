<?php
	$title = "MyHotels - Työntekijän tiedot";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 2) {
		$action = null;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		$customers = $user->getCustomers();
		
		switch($action) {
			case 'update':
				$user->doUpdateEmployee($_POST['etu'], $_POST['suku'], $_POST['email'], $_POST['salary'], $_POST['office'], $_POST['id']);
			default:
				$data = $user->getUserDataWithID($_POST['id'], true);
				require("Views/EmployeeInfo.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>