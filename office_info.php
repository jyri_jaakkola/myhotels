<?php
	$title = "MyHotels - Toimipisteen tiedot";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 2) {
		$action = null;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
	
	
		$officeInfo = $room->hotelWithID($_POST['id']);
		$employees = $user->getEmployees();
		$customers = $office->customersIn($_POST['id']);
		
		switch($action) {
			case 'delete':
				$office->deleteOffice($_POST['id']);
				header("Location: dashboard.php");
				break;
			case 'update':
				$office->updateOffice($_POST['id'], $_POST['nimi'], $_POST['director']);
				header("Location: dashboard.php");
				break;
			default:
				require("Views/HotelInfo.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>