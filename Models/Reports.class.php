<?php

class ReportsModel
{
	
    public function __construct() {
	}
	
	public function monthlySales($year, $month) {
		global $real_path;
		$url = $real_path ."api/salesreport.php?year=".$year."&month=".$month;
		$result = file_get_contents($url);
		$data = json_decode($result, true);
		return $data;
	}
	
	public function printLine($array) {
		echo "<tr>";
		foreach ($array as $a) {
			echo "<td>". $a ."</td>";
		}
		echo "</tr>\n";
	}
	
}

?>