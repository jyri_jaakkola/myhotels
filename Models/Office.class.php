<?php

class OfficeModel
{
	
    public function __construct() {
	}
	
	public function deleteOffice($id) {
		global $s;
		$db = connect();
		$delete = $db->prepare("DELETE FROM toimipiste WHERE toimipisteID=?");
		$delete->bindParam(1, $id);
		
		if ($delete->execute()) {
			$_SESSION['notification'] = $s['OFFICE_DEL_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['OFFICE_DEL_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function updateOffice($id, $name, $director) {
		global $s;
		$db = connect();
		$update = $db->prepare("UPDATE toimipiste SET nimi=?, johtajaID=? WHERE toimipisteID=?");
		$update->bindParam(1, $name);
		$update->bindParam(2, $director);
		$update->bindParam(3, $id);
		
		if ($update->execute()) {
			$_SESSION['notification'] = $s['OFFICE_UPD_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['OFFICE_UPD_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function newOffice($name, $director) {
		global $s;
		$db = connect();
		$insert = $db->prepare("INSERT INTO toimipiste (nimi, johtajaID, ketjuID) VALUES (?,?, 1)");
		$insert->bindParam(1, $name);
		$insert->bindParam(2, $director);
		
		if ($insert->execute()) {
			$_SESSION['notification'] = $s['OFFICE_NEW_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['OFFICE_NEW_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function customersIn($id) {
		$customers = array();
		
		$db = connect();
		$select = $db->prepare("SELECT	etu as etunimi, 
										suku as sukunimi, 
										email as email 
								FROM 	asiakas as a,
										toimipiste as tp,
										huone as h,
										huoneVaraus as hv
								WHERE
										tp.toimipisteID = ?
										AND
										hv.asiakasID = a.asiakasID 
										AND
										hv.HuoneID = h.huoneID
										AND
										hv.asiakasID = a.asiakasID
										AND
										h.toimipisteID = tp.toimipisteID
										AND
										hv.loppu >= NOW()
										AND
										hv.alku <= NOW()");
		$select->bindParam(1, $id);
		
		if ($select->execute()) {
			$row = $select->fetch(PDO::FETCH_ASSOC);
			$counter = 1;
			while ($row != null) {
				$customers[$counter] = array('first' => $row['etunimi'], 'last' => $row['sukunimi'], 'email' => $row['email']);
				$row = $select->fetch(PDO::FETCH_ASSOC);
				$counter++;
			}
		} else {
			$error_code = 10;
		}
		return $customers;
	}
	
}	

?>