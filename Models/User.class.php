<?php

class UserModel
{
	
    public function __construct() {
	}
	
	public function doLogin($email, $password) {
		global $s;
		if (!$this->isLogged()) {
			$result = $this->doCheckUser($email, $password);
			
			if (!$result) {
				$_SESSION['notification'] = $s['LOGIN_FAILED'];
				$_SESSION['success'] = false;
				return false;
			} else {
				if ($result['asiakasID'] == NULL) {
					$id = $result['tyontekijaID'];
					$role = $this->checkRole($id);
					if (is_array($role)) {
						$toimipiste = $role;
						$role = 3;
					}
				} else {
					$id = $result['asiakasID'];
					$role = 1;
				}
				$_SESSION['id'] = $id;
				$_SESSION['api-code'] = $result['api'];
				$_SESSION['role'] = $role;
				$_SESSION['notification'] = $s['LOGIN_SUCCESFUL'];
				$_SESSION['success'] = true;
				if($role == 3) {
					$_SESSION['office'] = $toimipiste;
				}
				header("Location: profile.php");

				return $id;
			}
		} else {
			$_SESSION['notification'] = $s['ALREADY_LOGGED'];
			$_SESSION['success'] = false;
			return true;
		}
    }
	
	public function doCheckUser($email, $password) {
		$db = connect();
		$db2 = connect();
		$asiakas = $db->prepare("SELECT asiakasID, salasana, api FROM asiakas WHERE email=?");
		$asiakas->bindParam(1, $email);
		$tyontekija = $db2->prepare("SELECT tyontekijaID, salasana, api FROM tyontekija WHERE email=?");
		$tyontekija->bindParam(1, $email);
		if ($asiakas->execute() && $tyontekija->execute()) {
			$row = $asiakas->fetch();
			if ($row == NULL) {
				$row = $tyontekija->fetch();
			}
			if($this->checkPassword($password, $row['salasana'])) {
				return $row;
			}
		} else {
			$error_code = 10;
		}
		return false;
	}
	
	public function doLogout() {
        unset($_SESSION['isLogged']);
		unset($_SESSION['id']);
		unset($_SESSION['api-code']);
		unset($_SESSION['role']);
		unset($_SESSION['office']);
        session_destroy();
		session_start();
		$_SESSION['notification'] = $s['LOGOUT_MESSAGE'];
		$_SESSION['success'] = true;
        return true;
    }
	
	public function isLogged() {
        if (isset($_SESSION['id'])){
            return $_SESSION['role'];
        } else {
            return false;
        }
    }
	
	public function getOffice() {
		$db = connect();
		$select = $db->prepare("SELECT toimipisteID FROM tyontekija WHERE tyontekijaID=?");
		$select->bindParam(1, $_SESSION['id']);
		
		if ($select->execute()) {
			$row = $select->fetch(PDO::FETCH_ASSOC);
			if ($row != NULL) {
				return $row['toimipisteID'];
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public function getUserData() {
		$db = connect();
		if ($this->isLogged() == 1) {
			$stmt = $db->prepare("SELECT etu, suku, email, salasana FROM asiakas WHERE asiakasID=?");
		} else {
			$stmt = $db->prepare("SELECT etu, suku, email, salasana FROM tyontekija WHERE tyontekijaID=?");
		}
		$stmt->bindParam(1, $_SESSION['id']);
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($row != NULL) {
				return $row;
			}
		} else {
			$error_code = 10;
		}
		
		return false;
	}
	
	public function getUserDataWithID($id, $tyontekija=false) {
		$db = connect();
		if (!$tyontekija) {
			$stmt = $db->prepare("SELECT asiakasID as id, etu, suku, email, salasana FROM asiakas WHERE asiakasID=?");
		} else {
			$stmt = $db->prepare("SELECT tyontekijaID as id, etu, suku, email, palkka, toimipisteID, salasana FROM tyontekija WHERE tyontekijaID=?");
		}
		$stmt->bindParam(1, $id);
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			if ($row != NULL) {
				return $row;
			}
		} else {
			$error_code = 10;
		}
		
		return false;
	}
	
	public function getEmployees() {
		$employees = array();
		$db = connect();
		$stmt = $db->prepare("SELECT tyontekijaID AS id, etu, suku FROM tyontekija");
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$id = $row['id'];
				$name = $row['etu'] . " " . $row['suku'];
				$employees[$id] = $name;
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		return $employees;
	}
	
	public function getCustomers() {
		$customers = array();
		$db = connect();
		$stmt = $db->prepare("SELECT asiakasID AS id, etu, suku, email FROM asiakas ORDER BY suku, etu");
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$id = $row['id'];
				$name = $row['etu'] . " " . $row['suku'] . " - " . $row['email'];
				$customers[$id] = $name;
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		return $customers;
	}
	
	public function checkRole($id) {
		$db = connect();
		$db2 = connect();
		$ketju = $db->prepare("SELECT johtajaID FROM ketju WHERE johtajaID=?");
		$ketju->bindParam(1, $id);
		$toimipiste = $db2->prepare("SELECT toimipisteID FROM toimipiste WHERE johtajaID=?");
		$toimipiste->bindParam(1, $id);
		if ($ketju->execute() && $toimipiste->execute()) {
			$row = $ketju->fetch();
			if ($row == NULL) {
				$row = $toimipiste->fetch();
				if($row == NULL) {
					return 2;
				} else {
					$toimipisteet = "";
					$toimipisteet .= $row['toimipisteID'];
					while(true) {
						$row = $toimipiste->fetch();
						if ($row == NULL) {
							break;
						} else {
							$toimipisteet .= "/". $row['toimipisteID'];
						}
					}
					$toimipisteet = explode("/", $toimipisteet);
					return $toimipisteet;
				}
			} else {
				return 4;
			}
		}
	}
	
	public function doUpdate($etu, $suku, $email, $old_pass, $new_pass) {
		global $error;
		global $s;
		$tyontekija = false;
		$id = $_SESSION['id'];
		if ($this->isLogged() > 1) {
			$tyontekija = true;
		}
		$userData = $this->getUserData();
		
		if ($this->checkPassword($old_pass, $userData['salasana'])) {
			if ($new_pass == NULL) {
				$new_pass = $old_pass;
			}
			$result = $this->updateUserData($id, $etu, $suku, $email, $new_pass, $tyontekija);
			if (!$result == 0) {
				$_SESSION['notification'] = $error[$result];
				$_SESSION['success'] = false;
				return;
			}
			$_SESSION['notification'] = $s['UPDATE_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $error[7];
			$_SESSION['success'] = false;
		}
		return;
	}
	
	public function updateUserData($id, $etu, $suku, $email, $sala, $tyontekija) {
		if (!$this->validateName($etu) || !$this->validateName($suku)) {
			return 2;
		}
		if (!$this->validateEmail($email)) {
			return 1;
		}
		if ($sala != NULL && !$this->passwordStrength($sala)) {
			return 5;
		}
		
		$result = $this->updateUserDatabase($etu, $suku, $email, $sala, $tyontekija);
		if ($result == 0) {
			return 0;
		} else {
			return $result;
		}
	}
	
	public function updateUserDatabase($etu, $suku, $email, $sala, $tyontekija) {
		$sala = $this->hashPassword($sala);
		$etu = utf8_decode($etu);
		$suku = utf8_decode($suku);
		$db = connect();
		if ($tyontekija) {
			$update = $db->prepare("UPDATE tyontekija SET etu=?, suku=?, email=?, salasana=? WHERE tyontekijaID=?");
		} else {
			$update = $db->prepare("UPDATE asiakas SET etu=?, suku=?, email=?, salasana=? WHERE asiakasID=?");
		}
		$update->bindParam(1, $etu);
		$update->bindParam(2, $suku);
		$update->bindParam(3, $email);
		$update->bindParam(4, $sala);
		$update->bindParam(5, $_SESSION['id']);
		
		if ($update->execute()) {
			return 0;
		} else {
			return 10;
		}
	}
	
	public function doUpdateWithID($id, $etu, $suku, $email, $tyontekija=false) {
		global $error;
		global $s;
		$id = $id;
	
		$result = $this->updateUserDataWithID($id, $etu, $suku, $email, $tyontekija);
		if (!$result == 0) {
			$_SESSION['notification'] = $error[$result];
			$_SESSION['success'] = false;
			return;
		}
		$_SESSION['notification'] = $s['UPDATE_SUCCESS'];
		$_SESSION['success'] = true;
		
		return;
	}
	
	public function updateUserDataWithID($id, $etu, $suku, $email, $tyontekija) {
		if (!$this->validateName($etu) || !$this->validateName($suku)) {
			return 2;
		}
		if (!$this->validateEmail($email)) {
			return 1;
		}
		
		$result = $this->updateUserDatabaseWithID($id, $etu, $suku, $email, $tyontekija);
		if ($result == 0) {
			return 0;
		} else {
			return $result;
		}
	}
	
	public function updateUserDatabaseWithID($id, $etu, $suku, $email, $tyontekija) {
		$etu = utf8_decode($etu);
		$suku = utf8_decode($suku);
		$db = connect();
		if ($tyontekija) {
			$update = $db->prepare("UPDATE tyontekija SET etu=?, suku=?, email=? WHERE tyontekijaID=?");
		} else {
			$update = $db->prepare("UPDATE asiakas SET etu=?, suku=?, email=? WHERE asiakasID=?");
		}
		$update->bindParam(1, $etu);
		$update->bindParam(2, $suku);
		$update->bindParam(3, $email);
		$update->bindParam(4, $id);
		
		if ($update->execute()) {
			return 0;
		} else {
			return 10;
		}
	}
	
	public function doUpdateEmployee($etu, $suku, $email, $palkka, $toimipiste, $id=NULL) {
		if ($id == NULL) {
			$id = $this->getLatestEmployeeID();
		}
		$db = connect();
		$update = $db->prepare("UPDATE tyontekija SET etu=?, suku=?, email=?, palkka=?, toimipisteID=? WHERE tyontekijaID=?");
		$update->bindParam(1, $etu);
		$update->bindParam(2, $suku);
		$update->bindParam(3, $email);
		$update->bindParam(4, $palkka);
		$update->bindParam(5, $toimipiste);
		$update->bindParam(6, $id);
		
		if ($update->execute()) {
			return;
		} else {
			$error_code = 10;
		}
		return;
	}

	public function deleteUser($id, $tyontekija) {
		global $s;
		$db = connect();
		if ($tyontekija) {
			$delete = $db->prepare("DELETE FROM tyontekija WHERE tyontekijaID=?");
		} else {
			$delete = $db->prepare("DELETE FROM asiakas WHERE asiakasID=?");
		}
		$delete->bindParam(1, $id);
		
		if ($delete->execute()) {
			$_SESSION['notification'] = $s['USER_DEL_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['USER_DEL_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
		
	}
	
	public function getLatestEmployeeID() {
		$db = connect();
		$select = $db->prepare("SELECT MAX(tyontekijaID) AS id FROM tyontekija");
		if ($select->execute()) {
			$row = $select->fetch(PDO::FETCH_ASSOC);
			return $row['id'];
		} else {
			$error_code = 10;
		}
		return 0;
	}
	
	public function doRegister($etu, $suku, $email, $synty, $sala, $tyontekija=false) {
		global $s;
		global $error;
		$result = $this->registerNewUser($etu, $suku, $email, $synty, $sala, $tyontekija);
		if (!$result == 0) {
			$_SESSION['notification'] = $error[$result];
			$_SESSION['success'] = false;
			return $result;
		}
		$_SESSION['notification'] = $s['REGISTER_SUCCESS'];
		$_SESSION['success'] = true;
		return $result;
	}
	
	public function registerNewUser($etu, $suku, $email, $synty, $sala, $tyontekija) {
		if (!$this->validateName($etu) || !$this->validateName($suku)) {
			// Jos nimet ovat invalideja
			return 2;
		}
		if (!$this->validateEmail($email)) {
			// Jos email on invalidi
			return 1;
		}
		if (!$this->checkEmailNotInDatabase($email)) {
			return 4;
		}
		if (!$this->validateBirthday($synty)) {
			return 3;
		}
		if ($sala != NULL && !$this->passwordStrength($sala)) {
			return 5;
		}
		
		if ($sala == NULL) {
			$sala = $synty;
		}
		$result = $this->registerDatabase($etu, $suku, $email, $synty, $sala, $tyontekija);
		if ($result == 0) {
			return 0;
		} else {
			return $result;
		}
	}
	
	public function registerDatabase($etu, $suku, $email, $synty, $sala, $tyontekija) {
		$sala = $this->hashPassword($sala);
		$api = $this->APIgenerator();
		$synty = $this->formatDate($synty);
		$etu = utf8_decode($etu);
		$suku = utf8_decode($suku);
		$db = connect();
		if ($tyontekija) {
			$insert = $db->prepare("INSERT INTO tyontekija (etu, suku, email, syntymaaika, salasana, api) VALUES (?, ?, ?, ?, ?, ?)");
		} else {
			$insert = $db->prepare("INSERT INTO asiakas (etu, suku, email, syntymaaika, salasana, api) VALUES (?, ?, ?, ?, ?, ?)");
		}
		$insert->bindParam(1, $etu);
		$insert->bindParam(2, $suku);
		$insert->bindParam(3, $email);
		$insert->bindParam(4, $synty);
		$insert->bindParam(5, $sala);
		$insert->bindParam(6, $api);
		
		if ($insert->execute()) {
			return 0;
		} else {
			return 10;
		}
	}
	
	public function userServices($id=NULL) {
		if ($id == NULL) {
			$id = $_SESSION['id'];
		}
		$services = array();
		$db = connect();
		$stmt = $db->prepare("SELECT 
									p.nimi AS palvelun_nimi,
									p.hinta AS palvelun_hinta, 
									pv.pvm AS paivamaara
							FROM 
									asiakas AS a,
									palvelu AS p, 
									palveluVaraus AS pv
							WHERE 
									a.asiakasID = ?
									AND p.palveluID = pv.palveluID
									AND pv.asiakasID = a.asiakasID
							ORDER BY pv.pvm DESC");
		$stmt->bindParam(1, $id);
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$services[] = array("nimi" => $row['palvelun_nimi'], "hinta" => $row['palvelun_hinta'], "pvm" => $row['paivamaara']);
				$row = $stmt->fetch(PDO:: FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		return $services;
	}
	
	public function userReservations($id=NULL) {
		if ($id == NULL) {
			$id = $_SESSION['id'];
		}
		$reservations = array();
		$db = connect();
		$stmt = $db->prepare("SELECT 
									h.tyyppi AS huoneenNimi, 
									h.hinta AS huoneenHinta, 
									hv.alku AS alkuPVM, 
									hv.loppu AS loppuPVM
							FROM 
									asiakas AS a, 
									huone AS h, 
									huoneVaraus AS hv
							WHERE 
									a.asiakasID = ?
									AND h.HuoneID = hv.huoneID
									AND hv.asiakasID = a.asiakasID
							ORDER BY hv.loppu DESC");
		$stmt->bindParam(1, $id);
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$reservations[] = array("nimi" => $row['huoneenNimi'], "hinta" => $row['huoneenHinta'], "alku" => $row['alkuPVM'], "loppu" => $row['loppuPVM']);	
				$row = $stmt->fetch(PDO:: FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		return $reservations;
	}
	
	public function validateName($name) {
		$rexSafety = "/^[^<,\"@{}()*$%?=>:|;#]*$/i";
		if (preg_match($rexSafety, $name)) {
			// NIMI ON VALIDI
			return true;
		} else {
			// NIMI ON INVALIDI
			return false;
		}
	}
	
	public function validateEmail($email) {
		if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function checkEmailNotInDatabase($email) {
		$db = connect();
		$db2 = connect();
		$select1 = $db->prepare("SELECT api FROM tyontekija WHERE email=?");
		$select2 = $db2->prepare("SELECT api FROM asiakas WHERE email=?");
		$select1->bindParam(1, $email);
		$select2->bindParam(1, $email);
		
		if ($select1->execute() && $select2->execute()) {
			$row = $select1->fetch();
			if ($row == NULL) {
				$row = $select2->fetch();
					if ($row == NULL) {
						return true;
					}
			}
		}
		return false;
	}
	
	public function validateBirthday($birth) {
		$birth = explode(".", $birth);
		if (count($birth) == 3) {
			$dd = $birth[0];
			$mm = $birth[1];
			$yy = $birth[2];
			if ($mm > 0 && $mm < 13 && $yy > 1850) {
				if ($mm == 1 || $mm == 3 || $mm == 5 || $mm = 7 || $mm == 8 || $mm = 10 || $mm == 12) {
					if ($dd > 0 && $dd < 32) {
						return true;
					}
				} else if ($mm == 4 || $mm == 6 || $mm == 9 || $mm == 11) {
					if ($dd > 0 && $dd < 31) {
						return true;
					}
				} else if ($mm == 2) {
					if (($yy%4 == 0 && $yy%100 != 0) || $yy%400 == 0) {
						if ($dd > 0 && $dd < 30) {
							return true;
						}
					} else {
						if ($dd > 0 && $dd < 29) {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public function formatDate($date) {
		$date = explode(".", $date);
		$dd = $date[0];
		$mm = $date[1];
		$yy = $date[2];
		return $yy ."-". $mm ."-". $dd;
	}
	
	public function passwordStrength($pass) {
		if (strlen($pass) >= 6) {
			if (preg_match("/[a-zåäö]/", $pass) && preg_match("/[A-ZÅÄÖ]/", $pass) && preg_match("/[0-9]/", $pass)) {
				return true;
			}
		}
		return false;
	}
	
	public function hashPassword($password){
        return password_hash($password, PASSWORD_BCRYPT);
    }
	
	public function checkPassword($password, $hash) {
		if (password_verify($password, $hash)) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getAPIcode() {
        if (isset($_SESSION['id'])){
            return $_SESSION['api-code'];
        } else {
            return false;
        }
    }
	
	public function APIgenerator(){
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
		$pass[0] = time();
        for ($i = 1; $i < 11; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass[$i] = $alphabet[$n];
        }
        $base = implode($pass);
		
		return md5($base);
    }
}

?>