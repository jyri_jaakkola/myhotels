<?php

class RoomModel
{
	
    public function __construct() {
	}
	
	public function availableRoomsJSON($hotel, $start_date, $end_date) {
		global $real_path;
		$url = $real_path ."api/freerooms.php?office=".$hotel."&start=".$start_date."&end=".$end_date;
		$result = file_get_contents($url);
		$data = json_decode($result, true);
		return $data;
	}
	
	public function availableRoom($hotel, $start_date, $end_date, $room_type) {
		$db = connect();
		$select = $db->prepare("SELECT h.huoneID AS id, h.tyyppi AS tyyppi, h.kuvaus AS kuvaus, h.hinta AS hinta
							  FROM huone AS h
							  WHERE h.huoneID NOT IN (
								SELECT hv.huoneID
								FROM huoneVaraus AS hv
								WHERE hv.loppu > ? AND hv.alku < ?
							  )
							  AND h.toimipisteID = ?
							  AND h.tyyppi = ?
							  LIMIT 1");
							  
		$select->bindParam(1, $start_date);
		$select->bindParam(2, $end_date);
		$select->bindParam(3, $hotel);
		$select->bindParam(4, $room_type);
		if ($select->execute()) {
			$result = $select->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
		return;
	}
	
	public function hotelList() {
		$db = connect();
		$select = $db->prepare("SELECT toimipisteID, nimi FROM toimipiste");
		if ($select->execute()) {
			$result = $select->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
		return;
	}
	
	public function hotelWithID($id) {
		$db = connect();
		$select = $db->prepare("SELECT toimipisteID, nimi, ketjuID, johtajaID FROM toimipiste WHERE toimipisteID=?");
		$select->bindParam(1, $id);
		if ($select->execute()) {
			$result = $select->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
		return;
	}
	
	public function roomsWithID($id) {
		$rooms = array();
		
		$db = connect();
		$select = $db->prepare("SELECT huoneID as id, nimi, tyyppi, kuvaus, hinta FROM huone WHERE toimipisteID=?");
		$select->bindParam(1, $id);
		
		if ($select->execute()) {
			$row = $select->fetch(PDO::FETCH_ASSOC);
			while ($row != null) {
				$rooms[$row['id']] = array("nimi" => $row['nimi'], "tyyppi" => $row['tyyppi'], "kuvaus" => $row['kuvaus'], "hinta" => $row['hinta']);
				$row = $select->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		
		return $rooms;
	}
	
	public function updateRoom($id, $type, $name, $desc, $price) {
		global $s;
		$type = utf8_decode($type);
		$name = utf8_decode($name);
		$desc = utf8_decode($desc);
		
		$db = connect();
		$update = $db->prepare("UPDATE huone SET tyyppi=?, nimi=?, kuvaus=?, hinta=? WHERE huoneID=?");
		$update->bindParam(1, $type);
		$update->bindParam(2, $name);
		$update->bindParam(3, $desc);
		$update->bindParam(4, $price);
		$update->bindParam(5, $id);
		
		if ($update->execute()) {
			$_SESSION['notification'] = $s['ROOM_UPD_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['ROOM_UPD_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function deleteRoom($id) {
		global $s;
		
		$db = connect();
		$delete = $db->prepare("DELETE FROM huone WHERE huoneID=?");
		$delete->bindParam(1, $id);
		
		if ($delete->execute()) {
			$_SESSION['notification'] = $s['ROOM_DEL_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['ROOM_DEL_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function newRoom($office, $type, $name, $desc, $price) {
		global $s;
		$type = utf8_decode($type);
		$name = utf8_decode($name);
		$desc = utf8_decode($desc);
		
		$db = connect();
		$insert = $db->prepare("INSERT INTO huone (toimipisteID, tyyppi, nimi, kuvaus, hinta) VALUES (?, ?, ?, ?, ?)");
		$insert->bindParam(1, $office);
		$insert->bindParam(2, $type);
		$insert->bindParam(3, $name);
		$insert->bindParam(4, $desc);
		$insert->bindParam(5, $price);
		
		if ($insert->execute()) {
			$_SESSION['notification'] = $s['NEW_ROOM_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['NEW_ROOM_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function makeReservation($start, $end, $office, $room, $id) {
		global $s;
		$db = connect();
		$insert = $db->prepare("INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES (?,?,?,?)");
		$insert->bindParam(1, $start);
		$insert->bindParam(2, $end);
		$insert->bindParam(3, $id);
		$available = $this->availableRoom($office, $start, $end, $room);
		$available = $available['id'];
		$insert->bindParam(4, $available);
		
		if ($insert->execute()) {
			$_SESSION['notification'] = $s['RESERVATION_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['RESERVATION_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
		return;
	}
	
	public function printRoom($r) {
		global $s;
		global $user;
		if ($r['hinta'] < 100) {
			echo "<img src='./Content/images/halpa.jpg' alt='Halpa' class='room-image' />\n";
		} else if ($r['hinta'] < 180) {
			echo "<img src='./Content/images/keski.jpg' alt='Keski' class='room-image' />\n";
		} else {
			echo "<img src='./Content/images/luxus.jpg' alt='Luxus' class='room-image' />\n";
		}
		echo "<h2>".$r['tyyppi']."</h2>\n";
		echo "<p>".$r['kuvaus']."</p>\n";
		echo "<h1>".$r['hinta']." &euro; / ".$s['NIGHT']."</h1>\n";
		echo "<p>Huoneita vapaana: ". $r['vapaana'] ." kpl</p>\n";
		
		echo "<button ";
		if ($user->isLogged() >= 1) {
			echo "onclick=\"window.location.href='make_reservation.php?start=". $_POST['from'] ."&end=". $_POST['to'] ."&office=". $_POST['hotel'] ."&room=". $r['tyyppi'] ."'\" class='button'>";
		} else {
			echo "class='button-not-logged-in'>";
		}
		echo $s['MAKE_RESERVATION']."</button>";
		
		echo "<div class='room-divider'></div>\n";
	}
}

?>