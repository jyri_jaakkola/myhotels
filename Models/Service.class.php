<?php

class ServiceModel
{
	
    public function __construct() {
	}
	
	public function servicesJSON($hotel) {
		global $real_path;
		$url = $real_path ."api/services.php?office=".$hotel;
		$result = file_get_contents($url);
		$data = json_decode($result, true);
		return $data;
	}
	
	public function serviceInfo($hotel, $name) {
		$db = connect();
		$select = $db->prepare("SELECT * FROM palvelu
								WHERE toimipisteID = ?
								AND nimi = ?
								LIMIT 1");
		$select->bindParam(1,$hotel);
		$select->bindParam(2,$name);
		if ($select->execute()) {
			$result = $select->fetch(PDO::FETCH_ASSOC);
			return $result;
		}
		return;
	}
	
	public function hotelList() {
		$db = connect();
		$select = $db->prepare("SELECT toimipisteID, nimi FROM toimipiste");
		if ($select->execute()) {
			$result = $select->fetchAll(PDO::FETCH_ASSOC);
			return $result;
		}
		return;
	}
	
	public function makeReservation($date, $office, $service, $id) {
		global $s;
		$db = connect();
		$insert = $db->prepare("INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES (?,?,?)");
		$insert->bindParam(1,$date);
		$insert->bindParam(2,$id);
		$palvelu = $this->serviceInfo($office, $service);
		$insert->bindParam(3,$palvelu['palveluID']);
		
		if ($insert->execute()) {
			$_SESSION['notification'] = $s['SERVICE_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['SERVICE_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
		return;
	}
	
	public function servicesWithID($id) {
		$services = array();
		
		$db = connect();
		$select = $db->prepare("SELECT palveluID as id, nimi, kuvaus, hinta FROM palvelu WHERE toimipisteID=?");
		$select->bindParam(1, $id);
		
		if ($select->execute()) {
			$row = $select->fetch(PDO::FETCH_ASSOC);
			while ($row != null) {
				$services[$row['id']] = array("nimi" => $row['nimi'], "kuvaus" => $row['kuvaus'], "hinta" => $row['hinta']);
				$row = $select->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		
		return $services;
	}
	
	public function updateService($id, $name, $desc, $price) {
		global $s;
		$name = utf8_decode($name);
		$desc = utf8_decode($desc);
		
		$db = connect();
		$update = $db->prepare("UPDATE palvelu SET nimi=?, kuvaus=?, hinta=? WHERE palveluID=?");
		$update->bindParam(1, $name);
		$update->bindParam(2, $desc);
		$update->bindParam(3, $price);
		$update->bindParam(4, $id);
		
		if ($update->execute()) {
			$_SESSION['notification'] = $s['SER_UPD_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['SER_UPD_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function deleteService($id) {
		global $s;
		
		$db = connect();
		$delete = $db->prepare("DELETE FROM palvelu WHERE palveluID=?");
		$delete->bindParam(1, $id);
		
		if ($delete->execute()) {
			$_SESSION['notification'] = $s['SER_DEL_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['SER_DEL_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function newService($office, $name, $desc, $price) {
		global $s;
		$name = utf8_decode($name);
		$desc = utf8_decode($desc);
		
		$db = connect();
		$insert = $db->prepare("INSERT INTO palvelu (toimipisteID, nimi, kuvaus, hinta) VALUES (?, ?, ?, ?)");
		$insert->bindParam(1, $office);
		$insert->bindParam(2, $name);
		$insert->bindParam(3, $desc);
		$insert->bindParam(4, $price);
		
		if ($insert->execute()) {
			$_SESSION['notification'] = $s['NEW_SER_SUCCESS'];
			$_SESSION['success'] = true;
		} else {
			$_SESSION['notification'] = $s['NEW_SER_FAIL'];
			$_SESSION['success'] = false;
			$error_code = 10;
		}
	}
	
	public function printService($r) {
		global $s;
		global $user;
		if ($r['nimi'] == 'Ravintola') {
			echo "<img src='./Content/images/restaurant.jpg' alt='restaurant' class='room-image' />\n";
		} else if ($r['nimi'] == 'Kuntosali') {
			echo "<img src='./Content/images/gym.jpg' alt='gym' class='room-image' />\n";
		} else if ($r['nimi'] == 'Sauna') {
			echo "<img src='./Content/images/sauna.jpg' alt='sauna' class='room-image' />\n";
		} else if ($r['nimi'] == 'Kauneushoitola') {
			echo "<img src='./Content/images/beauty.jpg' alt='beauty' class='room-image' />\n";
		} else {
			echo "<img src='./Content/images/no-image.jpg' alt='no-image' class='room-image' />\n";
		}
		echo "<h2>".$r['nimi']."</h2>\n";
		echo "<p>".$r['kuvaus']."</p>\n";
		echo "<h1>".$r['hinta']." &euro;</h1>\n";
		
		echo "<button ";
		if ($user->isLogged() >= 1) {
			echo "onclick=\"window.location.href='make_service_reservation.php?date=". $_POST['date'] ."&office=". $_POST['hotel'] ."&service=". $r['nimi'] ."'\" class='button'>";
		} else {
			echo "class='button-not-logged-in'>";
		}
		echo $s['MAKE_RESERVATION']."</button>";
		
		echo "<div class='room-divider'></div>\n";
	}
}

?>