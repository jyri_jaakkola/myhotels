<?php
	require("config.php");
	
	if ($user->doLogin($_POST['user'], $_POST['pass'])) {
		header("Location: profile.php");
	} else {
		header("Location: index.php");
	}
?>