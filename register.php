<?php
	$title = "MyHotels - Rekisteröidy";
	require("config.php");
	
	if (isset($_GET['action'])) {
		$action = $_GET['action'];
	} else {
		$action = NULL;
	}
	
	switch($action) {
		case 'register':
			if ($_POST['pass'] == $_POST['repeat']) {
				$user->doRegister($_POST['first'], $_POST['last'], $_POST['email'], $_POST['birth'], $_POST['pass']);
				if ($_SESSION['success'] == true) {
					header("location: index.php");
				}
			} else {
				$_SESSION['notification'] = $error[6];
				$_SESSION['success'] = false;
			}
			break;
		default:
			require("Views/Register.view.php");
	}
	if ($_SESSION['success'] != true) {
		require("Views/Register.view.php");
	}
?>