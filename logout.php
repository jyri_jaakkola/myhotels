<?php
	require("config.php");
	
	$user->doLogout();
	
	$_SESSION['notification'] = $s['LOGOUT_MESSAGE'];
	$_SESSION['success'] = true;
	header("Location: index.php");
?>