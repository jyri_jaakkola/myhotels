<?php
	header('Content-Type: application/json');
	require("API.class.php");
	$api = new APIModel();
	
	$office = 1;
	$start_date = date('Y-m-d');
	$end_date = date('Y-m-d');

	if(isset($_GET['office'])) {
		if (is_numeric($_GET['office'])) {
			$office = $_GET['office'];
		}
	}
	if(isset($_GET['start'])) {
		$start_date = $_GET['start'];
	}
	if(isset($_GET['end'])) {
		$end_date = $_GET['end'];
	}

	
	$result = $api->freeRooms($office, $start_date, $end_date);
	
	echo json_encode($result);
?>