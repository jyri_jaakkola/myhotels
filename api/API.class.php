<?php

class APIModel {
	public function __construct() {
	}
	
	private function connect() {
		return new PDO('mysql:host=localhost;dbname=myhotels', 'user', 'ywSYHVM9EPrCUCzh');
	}
	
	public function freeRooms($toimipiste, $start_date, $end_date) {
		$db = $this->connect();
		$stmt = $db->prepare("SELECT h.tyyppi AS tyyppi, h.kuvaus AS kuvaus, h.hinta AS hinta, COUNT(h.tyyppi) AS vapaana, tp.nimi AS hotelli
							  FROM huone AS h, toimipiste AS tp
							  WHERE h.huoneID NOT IN (
								SELECT hv.huoneID
								FROM huoneVaraus AS hv
								WHERE hv.loppu > ? AND hv.alku < ?
							  )
							  AND h.toimipisteID = ?
							  AND h.toimipisteID = tp.toimipisteID
							  GROUP BY h.tyyppi
							  ORDER BY h.hinta;");
		$stmt->bindParam(1, $start_date);
		$stmt->bindParam(2, $end_date);
		$stmt->bindParam(3, $toimipiste);
		
		if ($stmt->execute()) {
			$result = array();
			$counter = 1;
			while ( ($row = $stmt->fetch(PDO::FETCH_ASSOC)) != NULL ) {
				$result[$counter] = array('tyyppi'=>utf8_encode($row['tyyppi']), 'kuvaus'=>utf8_encode($row['kuvaus']), 'hinta'=>utf8_encode($row['hinta']), 'vapaana'=>$row['vapaana'], 'hotelli'=>utf8_encode($row['hotelli']));
				$counter++;
			}
			if ($result != NULL) {
				return $result;
			}
		} else {
			$error_code = 10;
		}
	}
	
	public function salesReport($year, $month) {
		$all_offices = $this->officesArray();
		$db = $this->connect();
		$stmt = $db->prepare("SELECT tp.toimipisteID AS id, SUM(p.hinta) AS palveluMyynti, 0 AS 'huoneMyynti'
							  FROM toimipiste AS tp, palvelu AS p, palveluVaraus AS pv
							  WHERE 	tp.toimipisteID = p.toimipisteID AND
										pv.palveluID = p.palveluID AND
										pv.varausID IN (
											SELECT pv.varausID
											FROM palveluVaraus AS pv
											WHERE MONTH(pv.pvm) = :month AND YEAR(pv.pvm) = :year
										)
							  GROUP BY tp.toimipisteID
							  UNION
							  SELECT tp.toimipisteID AS id, 0 AS palveluMyynti, SUM(DATEDIFF(hv.loppu, hv.alku)*h.hinta) AS huoneMyynti
							  FROM toimipiste AS tp, huone AS h, huoneVaraus AS hv
							  WHERE  tp.toimipisteID = h.toimipisteID AND
										hv.huoneID = h.huoneID AND
										hv.varausID IN (
											SELECT hv.varausID
											FROM huoneVaraus AS hv
											WHERE MONTH(hv.loppu) = :month AND YEAR(hv.loppu) = :year
										)
							  GROUP BY tp.toimipisteID");
							  
		$stmt->bindParam(':month', $month);
		$stmt->bindParam(':year', $year);
		
		
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$all_offices[$row['id']]['palvelu'] += $row['palveluMyynti'];
				$all_offices[$row['id']]['huone'] += $row['huoneMyynti'];
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		
		return $all_offices;
	}
	
	public function officesArray() {
		$db = $this->connect();
		$stmt = $db->prepare("SELECT toimipisteID, nimi FROM toimipiste");
		$offices = array();
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while($row != NULL) {
				$offices[$row['toimipisteID']] = array('nimi' => $row['nimi'], 'palvelu' => 0, 'huone' => 0);
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		
		return $offices;
	}
	
	public function services($office) {
		$all_services = array();
		$db = $this->connect();
		$stmt = $db->prepare("SELECT palveluID AS id, nimi, kuvaus, hinta FROM palvelu WHERE toimipisteID=?");
		$stmt->bindParam(1, $office);
		
		if ($stmt->execute()) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			while ($row != NULL) {
				$all_services[$row['id']] = array("nimi" => utf8_encode($row['nimi']), "kuvaus" => utf8_encode($row['kuvaus']), "hinta" => utf8_encode($row['hinta']));
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
			}
		} else {
			$error_code = 10;
		}
		
		return $all_services;
	}

}

?>