<?php
	header('Content-Type: application/json');
	require("API.class.php");
	$api = new APIModel();
	
	$year = 2014;
	$month = 1;
			
	if(isset($_GET['month'])) {
		if (is_numeric($_GET['month'])) {
			$month = $_GET['month'];
		}
	}
	if(isset($_GET['year'])) {
		if (is_numeric($_GET['year'])) {
			$year = $_GET['year'];
		}
	}

	$result = $api->salesReport($year, $month);
	
	echo json_encode($result);
?>