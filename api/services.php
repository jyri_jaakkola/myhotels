<?php
	header('Content-Type: application/json');
	require("API.class.php");
	$api = new APIModel();
	
	$office = 1;

	if(isset($_GET['office'])) {
		if (is_numeric($_GET['office'])) {
			$office = $_GET['office'];
		}
	}
	
	$result = $api->services($office);
	
	echo json_encode($result);
?>