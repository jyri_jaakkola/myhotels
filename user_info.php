<?php
	$title = "MyHotels - Asiakas";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 1) {
		$id = 0;
		$action = NULL;
		
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
		}
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		$services = $user->userServices($id);
		$reservations = $user->userReservations($id);
		
		
		switch($action) {
			case 'update':
				$user->doUpdateWithID($id, $_POST['etu'], $_POST['suku'], $_POST['email']);
				$data = $user->getUserDataWithID($id);
				require("Views/Reservations.view.php");
				break;
			default:
				$data = $user->getUserDataWithID($id);
				require("Views/Reservations.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>