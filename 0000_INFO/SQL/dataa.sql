-- TYÖNTEKIJÄT --

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Johan', 'Johtaja', 'johan.johtaja@myhotels.fi', 6900, '1954-6-18', '05fc5518e04364404b435265653d7e74', '$2y$10$FzUYoeWes6klrerq32G19.Ectgas2963aKM2/ISa6JYyxPvMnSdqG');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Make', 'Siivooja', 'make.siivooja@myhotels.fi', 4900, '1983-8-21', 'aec40af60cc80703e35ce67c82621a25', '$2y$10$TLU1CyEQC2qDLCxG9PY0.ujt8nkiBWGg/SxM.kiyX8VFOGN9cy2g2');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Maiju', 'Siivooja', 'maiju.siivooja@myhotels.fi', 1300, '1981-12-1', 'c1f8bb155887d3934526a2a40ad5cb0b', '$2y$10$aAtYDX4jvLka8duqZxenfuhKwjMqySxu.XygoTVP8Y1p2YMQMX3sK');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Mike', 'Markkinoija', 'mike.markkinoija@myhotels.fi', 4400, '1959-11-7', '613efbd83a31ae045c6eb5e312724f4c', '$2y$10$oECygyn7Je.YmKkBg/4fse4tPUJUn9keSoXUcwfb974tXd.Ej6sSm');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Masa', 'Markkina', 'masa.markkina@myhotels.fi', 5900, '1951-5-25', '23fc4acde2a634696d60d1d757f96e8b', '$2y$10$Xc5S/pF3kOI7tTyWzUk/sehaHte3E/D1xXVAO0kU2THIv07HKNhKm');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Tiina', 'Tuote', 'tiina.tuote@myhotels.fi', 1000, '1955-6-17', '1eb3fe5012e9fae09da1f3e8820fc4f7', '$2y$10$aKl54y7ggqWtaFSQFP6y2u4Ckayqern2N9eaawJ3maaWyHBnUfS5S');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Taisto', 'Tähdellinen', 'taisto.tahdellinen@myhotels.fi', 5300, '1977-11-16', '70bd83b67f03748f0507ed1552302c75', '$2y$10$KiwjuWFoMJtwDnhXzMK3k.uOAP7kqqQup4Lh/ByqUNySmK2Kg34ia');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Jaana', 'Jämäkkä', 'jaana.jamakka@myhotels.fi', 6400, '1980-7-28', 'b4e43692fcb24c6f4538b679f04a7381', '$2y$10$BdIG4OM8F6qdmdfHaG1YJeJv0EmVR95BdTOxdQ8K2Iuv5m3Ln/cfW');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Ismo', 'Insinööri', 'ismo.insinoori@myhotels.fi', 5900, '1962-11-16', '651e15cb7fd6d6ad13596f989b426741', '$2y$10$wYXdzUe5JdMBVBuN.qVCuub7T.ywo4t7QChw.PBy3PkmYe.O2hJfi');

INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) 
VALUES ('Peke', 'Pomo', 'peke.pomo@myhotels.fi', 6300, '1979-6-25', 'cf0bee0deafd865aa11cf9db3aa39a51', '$2y$10$v4LFPIq5Rim/STeZMWKUvO81R6PuzXlv.H3WSQaD69.7hRncEi4BO');

-- ASIAKKAAT --

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Unelma', 'Säleikkö', 'unelma.saleikko@asiakas.fi', '1991-1-8', '37ff35cda57cc5e5718d8ca250c86c92', '$2y$10$bhnqw5UGCpbTh2utd/ZEZOqe4nBTnHeD6SaHXq1MUtoaq1TZP.p3G');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Inkeri', 'Möttönen', 'inkeri.mottonen@asiakas.fi', '1969-4-19', '42a7fb55d7957fca84fbebd4f9a43535', '$2y$10$HWUUA3rJuUp3G5w5uuw2JO5HYueS8D6NAi5geziZGxHQgnY1emqqS');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Kaarina', 'Virtanen', 'kaarina.virtanen@asiakas.fi', '1979-9-17', 'c2cd054beb6891f41c199b20c9ad9322', '$2y$10$gJpfJDvkzzt21zZemdcVFOLNetfcJUkjG5oCO5TA8go75sbyjir52');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Daniel', 'Järvinen', 'daniel.jarvinen@asiakas.fi', '1967-6-24', '4fd28a92307895c1c4027c1cb3b78683', '$2y$10$MYQMAjq4EbQ/ZPhqWnXO6O3xWmrRzcIsjNTLf3oHbYphj1Y1fVlyO');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Viljami', 'Ketonen', 'viljami.ketonen@asiakas.fi', '1969-6-14', '13cb945b75c97bf75e7bd94d77091566', '$2y$10$ceFjA3srihjezmCFJwU9NeizTOnb0Fa64zScTD9AO99aqaemZmv0.');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Elmeri', 'Pesonen', 'elmeri.pesonen@asiakas.fi', '1984-1-5', '4561066cc6da209cc0bd62c691824c56', '$2y$10$0blF0HwaNCOiG8I2/4xdnuNYbhdbUFbCyFMB7bxJqHPDRSdN540Si');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Elmo', 'Matikka', 'elmo.matikka@asiakas.fi', '1972-9-25', '8f9132eff896b317d22c33bd960a2012', '$2y$10$Wf7VFDFkmn.JnQDOcXKZm.VY1QEOZzM/MhK8jZhLGgHZkDuh41J9.');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Riina', 'Isomäki', 'riina.isomaki@asiakas.fi', '1989-1-17', '659b0315c391dfc574361dfb8dc73a5c', '$2y$10$uKt.JFW/ycuzkH5a5bfVO.LGJFQyhT5cq67gnGm/rN8QIpSJ1asMa');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Pertti', 'Perustelija', 'pertti.perustelija@asiakas.fi', '1980-11-17', '12cde1826558f9a3c8a09cf0c98c279d', '$2y$10$gJjoFnIsT4PTYa9u38HQOOD94.jmZfPzGlJr/qNopB/d5nvn3lDNG');

INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) 
VALUES ('Tapio', 'Suominen', 'tapio.suominen@asiakas.fi', '1982-7-27', '946807b72caeebb807e753038043ae26', '$2y$10$3dGuithZeKBrWyjZqL9GmOXijsaJCHr2gJZG5MMQHqD2mkov2cr8y');

-- KETJU --

INSERT INTO ketju (nimi, johtajaID) VALUES ('MyHotels', 1);

-- TOIMIPISTEET --

INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Staten Hotelli', 1, 3);

INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Atomic Hotelli', 1, 3);

INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Fisher Hotelli', 1, 3);

-- HUONEET --

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 1', 'Tämä huone on halpa.', 50, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 2', 'Tämä huone on halpa.', 50, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 3', 'Tämä huone on halpa.', 50, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 1', 'Tämä huone on keskihintainen.', 110, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 2', 'Tämä huone on keskihintainen.', 110, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 3', 'Tämä huone on keskihintainen.', 110, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 1', 'Tämä huone on kallis.', 200, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 2', 'Tämä huone on kallis.', 200, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 3', 'Tämä huone on kallis.', 200, 1);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 1', 'Tämä huone on halpa.', 50, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 2', 'Tämä huone on halpa.', 50, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 3', 'Tämä huone on halpa.', 50, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 1', 'Tämä huone on keskihintainen.', 110, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 2', 'Tämä huone on keskihintainen.', 110, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 3', 'Tämä huone on keskihintainen.', 110, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 1', 'Tämä huone on kallis.', 200, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 2', 'Tämä huone on kallis.', 200, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 3', 'Tämä huone on kallis.', 200, 2);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 1', 'Tämä huone on halpa.', 50, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 2', 'Tämä huone on halpa.', 50, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis', 'Halppis 3', 'Tämä huone on halpa.', 50, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 1', 'Tämä huone on keskihintainen.', 110, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 2', 'Tämä huone on keskihintainen.', 110, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus', 'Perus 3', 'Tämä huone on keskihintainen.', 110, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 1', 'Tämä huone on kallis.', 200, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 2', 'Tämä huone on kallis.', 200, 3);

INSERT INTO huone (tyyppi, nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus', 'Luxus 3', 'Tämä huone on kallis.', 200, 3);

-- PALVELUT --

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Ravintola', 'Ravintola tarjoaa ruokaa.', 20, 1);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kuntosali', 'Kuntosalilla voit treenata.', 5, 1);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Sauna', 'Saunassa on lämmin.', 5, 1);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kauneushoitola', 'Kauneushoitolassa kauneus kuntoon.', 30, 1);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Ravintola', 'Ravintola tarjoaa ruokaa.', 20, 2);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kuntosali', 'Kuntosalilla voit treenata.', 5, 2);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Sauna', 'Saunassa on lämmin.', 5, 2);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kauneushoitola', 'Kauneushoitolassa kauneus kuntoon.', 30, 2);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Ravintola', 'Ravintola tarjoaa ruokaa.', 20, 3);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kuntosali', 'Kuntosalilla voit treenata.', 5, 3);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Sauna', 'Saunassa on lämmin.', 5, 3);

INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kauneushoitola', 'Kauneushoitolassa kauneus kuntoon.', 30, 3);

-- HUONEVARAUS --

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-7-2', '2014-7-7', 8, 12);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-9-8', '2014-9-13', 9, 14);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-12-2', '2014-12-7', 1, 19);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-4-12', '2014-4-17', 1, 24);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-5-1', '2014-5-6', 7, 26);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-9-7', '2014-9-12', 1, 15);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-3-6', '2014-3-11', 7, 3);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-9-13', '2014-9-18', 4, 7);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-12-1', '2014-12-6', 7, 17);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-7-8', '2014-7-13', 1, 15);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-7-2', '2014-7-7', 3, 24);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-11-5', '2014-11-10', 8, 8);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-4-6', '2014-4-11', 2, 27);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-10-5', '2014-10-10', 6, 26);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-9-3', '2014-9-8', 1, 11);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-12-6', '2014-12-11', 7, 26);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-5-5', '2014-5-10', 6, 26);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-10-10', '2014-10-15', 5, 9);

INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-9-11', '2014-9-16', 3, 17);

-- PALVELUVARAUS --

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-12-27', 9, 4);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-4-3', 3, 1);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-5-22', 10, 1);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-11-1', 5, 11);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-5-2', 9, 9);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-4-11', 7, 2);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-12-4', 5, 9);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-10-19', 3, 10);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-8-5', 1, 11);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-3-8', 10, 8);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-1-25', 7, 12);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-11-2', 9, 3);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-1-19', 10, 5);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-1-19', 6, 12);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-10-28', 7, 7);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-8-27', 4, 3);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-2-11', 1, 5);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-8-28', 10, 8);

INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-11-17', 6, 10);

-- TYONTEKIJÖILLE TOIMIPISTEID:T --

UPDATE tyontekija SET toimipisteID=1 WHERE tyontekijaID BETWEEN 1 AND 4;

UPDATE tyontekija SET toimipisteID=2 WHERE tyontekijaID BETWEEN 5 AND 7;

UPDATE tyontekija SET toimipisteID=1 WHERE tyontekijaID BETWEEN 8 AND 10;