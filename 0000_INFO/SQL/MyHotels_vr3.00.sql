-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema myhotels
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema myhotels
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `myhotels` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `myhotels` ;

-- -----------------------------------------------------
-- Table `myhotels`.`toimipiste`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`toimipiste` (
  `toimipisteID` INT NOT NULL AUTO_INCREMENT,
  `nimi` VARCHAR(45) NULL,
  `ketjuID` INT NULL,
  `johtajaID` INT NULL,
  PRIMARY KEY (`toimipisteID`),
  INDEX `KetjuID_idx` (`ketjuID` ASC),
  INDEX `JohtajaID_idx` (`johtajaID` ASC),
  CONSTRAINT `ketjuID_toimi`
    FOREIGN KEY (`ketjuID`)
    REFERENCES `myhotels`.`ketju` (`ketjuID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `johtajaID_toimi`
    FOREIGN KEY (`johtajaID`)
    REFERENCES `myhotels`.`tyontekija` (`tyontekijaID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`tyontekija`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`tyontekija` (
  `tyontekijaID` INT NOT NULL AUTO_INCREMENT,
  `etu` VARCHAR(45) NULL,
  `suku` VARCHAR(45) NULL,
  `palkka` DOUBLE NULL,
  `email` VARCHAR(100) NULL,
  `syntymaaika` DATE NULL,
  `salasana` VARCHAR(100) NULL,
  `api` VARCHAR(32) NULL,
  `toimipisteID` INT NULL,
  PRIMARY KEY (`tyontekijaID`),
  INDEX `ToimipisteID_idx` (`toimipisteID` ASC),
  CONSTRAINT `toimipisteID_tyontekija`
    FOREIGN KEY (`toimipisteID`)
    REFERENCES `myhotels`.`toimipiste` (`toimipisteID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`ketju`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`ketju` (
  `ketjuID` INT NOT NULL AUTO_INCREMENT,
  `nimi` VARCHAR(45) NULL,
  `johtajaID` INT NULL,
  PRIMARY KEY (`ketjuID`),
  INDEX `JohtajaID_idx` (`johtajaID` ASC),
  CONSTRAINT `johtajaID_ketju`
    FOREIGN KEY (`johtajaID`)
    REFERENCES `myhotels`.`tyontekija` (`tyontekijaID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`huone`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`huone` (
  `huoneID` INT NOT NULL AUTO_INCREMENT,
  `tyyppi` VARCHAR(45) NULL,
  `nimi` VARCHAR(45) NULL,
  `kuvaus` VARCHAR(120) NULL,
  `hinta` DOUBLE NULL,
  `toimipisteID` INT NULL,
  PRIMARY KEY (`huoneID`),
  INDEX `ToimipisteID_idx` (`toimipisteID` ASC),
  CONSTRAINT `toimipisteID_huone`
    FOREIGN KEY (`toimipisteID`)
    REFERENCES `myhotels`.`toimipiste` (`toimipisteID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`palvelu`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`palvelu` (
  `palveluID` INT NOT NULL AUTO_INCREMENT,
  `nimi` VARCHAR(45) NULL,
  `kuvaus` VARCHAR(120) NULL,
  `hinta` DOUBLE NULL,
  `toimipisteID` INT NULL,
  PRIMARY KEY (`palveluID`),
  INDEX `ToimipisteID_idx` (`toimipisteID` ASC),
  CONSTRAINT `toimipisteID_palvelu`
    FOREIGN KEY (`toimipisteID`)
    REFERENCES `myhotels`.`toimipiste` (`toimipisteID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`asiakas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`asiakas` (
  `asiakasID` INT NOT NULL AUTO_INCREMENT,
  `etu` VARCHAR(45) NULL,
  `suku` VARCHAR(45) NULL,
  `email` VARCHAR(100) NULL,
  `syntymaaika` DATE NULL,
  `salasana` VARCHAR(100) NULL,
  `api` VARCHAR(32) NULL,
  PRIMARY KEY (`asiakasID`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`huoneVaraus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`huoneVaraus` (
  `varausID` INT NOT NULL AUTO_INCREMENT,
  `alku` DATE NULL,
  `loppu` DATE NULL,
  `asiakasID` INT NULL,
  `huoneID` INT NULL,
  PRIMARY KEY (`varausID`),
  INDEX `AsiakasID_idx` (`asiakasID` ASC),
  INDEX `HuoneID_idx` (`huoneID` ASC),
  CONSTRAINT `asiakasID_huonev`
    FOREIGN KEY (`asiakasID`)
    REFERENCES `myhotels`.`asiakas` (`asiakasID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `huoneID_huonev`
    FOREIGN KEY (`huoneID`)
    REFERENCES `myhotels`.`huone` (`huoneID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`palveluVaraus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`palveluVaraus` (
  `varausID` INT NOT NULL AUTO_INCREMENT,
  `pvm` DATE NULL,
  `asiakasID` INT NULL,
  `palveluID` INT NULL,
  PRIMARY KEY (`varausID`),
  INDEX `AsiakasID_idx` (`asiakasID` ASC),
  INDEX `PalveluID_idx` (`palveluID` ASC),
  CONSTRAINT `asiakasID_palveluv`
    FOREIGN KEY (`asiakasID`)
    REFERENCES `myhotels`.`asiakas` (`asiakasID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `palveluID_palveluv`
    FOREIGN KEY (`palveluID`)
    REFERENCES `myhotels`.`palvelu` (`palveluID`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`huoneLaskutetut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`huoneLaskutetut` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `summa` DOUBLE NULL,
  `pvm` DATE NULL,
  `toimipisteID` INT NULL,
  `asiakasID` INT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_laskutetut_toimipiste_idx` (`toimipisteID` ASC),
  INDEX `FK_laskutetut_asiakas_idx` (`asiakasID` ASC),
  CONSTRAINT `FK_laskutetut_toimipiste`
    FOREIGN KEY (`toimipisteID`)
    REFERENCES `myhotels`.`toimipiste` (`toimipisteID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_laskutetut_asiakas`
    FOREIGN KEY (`asiakasID`)
    REFERENCES `myhotels`.`asiakas` (`asiakasID`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `myhotels`.`palveluLaskutetut`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `myhotels`.`palveluLaskutetut` (
  `ID` INT NOT NULL AUTO_INCREMENT,
  `summa` DOUBLE NULL,
  `pvm` DATE NULL,
  `toimipisteID` INT NULL,
  PRIMARY KEY (`ID`),
  INDEX `FK_palvelulaskutettu_idx` (`toimipisteID` ASC),
  CONSTRAINT `FK_palvelulaskutettu`
    FOREIGN KEY (`toimipisteID`)
    REFERENCES `myhotels`.`toimipiste` (`toimipisteID`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
