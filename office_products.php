<?php
	$title = "MyHotels - Toimipisteen tuotteet";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 2) {
		if(isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = NULL;
		}
		
		switch($action) {
			case 'update':
				$room->updateRoom($_POST['id'], $_POST['type'], $_POST['name'], $_POST['desc'], $_POST['price']);
				$rooms = $room->roomsWithID($_POST['tid']);
				$services = $service->servicesWithID($_POST['tid']);
				require("Views/HotelProducts.view.php");
				break;
			case 'updates':
				$service->updateService($_POST['id'], $_POST['name'], $_POST['desc'], $_POST['price']);
				$rooms = $room->roomsWithID($_POST['tid']);
				$services = $service->servicesWithID($_POST['tid']);
				require("Views/HotelProducts.view.php");
				break;
			case 'delete':
				$room->deleteRoom($_POST['id']);
				$rooms = $room->roomsWithID($_POST['tid']);
				$services = $service->servicesWithID($_POST['tid']);
				require("Views/HotelProducts.view.php");
				break;
			case 'deletes':
				$service->deleteService($_POST['id']);
				$rooms = $room->roomsWithID($_POST['tid']);
				$services = $service->servicesWithID($_POST['tid']);
				require("Views/HotelProducts.view.php");
				break;
			default:
				$rooms = $room->roomsWithID($_POST['tid']);
				$services = $service->servicesWithID($_POST['tid']);
				require("Views/HotelProducts.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>