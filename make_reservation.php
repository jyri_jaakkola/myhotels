<?php
	$title = "MyHotels - Tee varaus";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] == 1) {
		$action = NULL;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		switch($action) {
			case 'confirm':
				$room->makeReservation($_POST['start'], $_POST['end'], $_POST['office'], $_POST['room'], $_SESSION['id']);
				header("Location: index.php");
				break;
			default:
				$data = $room->availableRoom($_GET['office'], $_GET['start'], $_GET['end'], $_GET['room']);
				$user_data = $user->getUserData();
				require("Views/Reservation.confirm.view.php");
		}
		
	} else if ($_SESSION['role'] > 1) {
		$action = NULL;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		switch($action) {
			case 'confirm':
				$room->makeReservation($_POST['start'], $_POST['end'], $_POST['office'], $_POST['room'], $_POST['id']);
				header("Location: index.php");
				break;
			default:
				$data = $room->availableRoom($_GET['office'], $_GET['start'], $_GET['end'], $_GET['room']);
				$customers = $user->getCustomers();
				require("Views/Reservation.confirm.employee.view.php");
		}
	} else {
		header("Location: index.php");
	}
?>