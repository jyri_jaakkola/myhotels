<?php
	$title = "MyHotels - Varaukset";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] == 1) {
		$services = $user->userServices();
		$reservations = $user->userReservations();
		
		
		switch($_GET) {
			default:
				require("Views/Reservations.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>