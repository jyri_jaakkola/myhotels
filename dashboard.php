<?php
	$title = "MyHotels - Hallintapaneeli";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] > 1) {
		$action = null;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		
		
		switch($action) {
			case 'new_customer':
				$user->doRegister($_POST['first'], $_POST['last'], $_POST['email'], $_POST['birth'], NULL);
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
				break;
			case 'new_employee':
				$check = $user->doRegister($_POST['first'], $_POST['last'], $_POST['email'], $_POST['birth'], NULL, true);
				if ($check == 0) {
					$user->doUpdateEmployee($_POST['first'], $_POST['last'], $_POST['email'], $_POST['salary'], $_POST['office']);
				}
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
				break;
			case 'new_office':
				$office->newOffice($_POST['office'], $_POST['manager']);
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
				break;
			case 'new_room':
				$room->newRoom($_POST['office-id'], $_POST['type'], $_POST['room_name'], $_POST['room_desc'], $_POST['room_price']);
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
				break;
			case 'new_service':
				$service->newService($_POST['office-id-service'], $_POST['service_name'], $_POST['service_desc'], $_POST['service_price']);
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
				break;
			default:
				$customers = $user->getCustomers();
				$employees = $user->getEmployees();
				$offices = $room->hotelList();
				require("Views/Dashboard.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>