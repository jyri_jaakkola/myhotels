		<h1>Tervetuloa!</h1>

		<img src="Content/images/hotel-reception.jpg" alt="Hotel Reception" class="img-right" />

		<p>T&auml;m&auml; on MyHotels -hotelliketjun kotisivu.
		   T&auml;&auml;ll&auml; voit tutustua eri hotelliemme huone- ja
		   palvelutarjontaan ja rekister&ouml;itym&auml;ll&auml; voit my&ouml;s
		   tehd&auml; varauksia suoraan sivuston kautta.</p>
		   
		<p>Toivotan mukavia hetki&auml; sivustolla ja hotelleissamme,
		   <div class="allekirjoitus">Johan Johtaja</div></p>
