<?php
	$etu = $data['etu'];
	$etu = utf8_encode($etu);
	$suku = $data['suku'];
	$suku = utf8_encode($suku);
	$email = $data['email'];
?>
		
		<h2>Tervetuloa <?=htmlspecialchars($etu)?> <?=htmlspecialchars($suku)?>!</h2>
		
        <p>Hienoa nähdä sinut taas! Kirjautumisesi suoritettiin onnistuneesti.<br>
           Klikkaa lukon kuvaketta kirjautuaksesi ulos tai palataksesi myöhemmin takaisin tälle sivulle.</p>
		   
		<div class="divider"></div>
		
		<h2>Muokkaa profiilia</h2>
		
		<form method="POST" action="profile.php?action=update">
			<table>
				<tr><td><?=$s['FIRST_NAME']?></td><td><input type="text" name="etu" class="input-field" value="<?=htmlspecialchars($etu)?>" /></td></tr>
				<tr><td><?=$s['LAST_NAME']?></td><td><input type="text" name="suku" class="input-field" value="<?=htmlspecialchars($suku)?>" /></td></tr>
				<tr><td><?=$s['EMAIL']?></td><td><input type="text" name="email" class="input-field" value="<?=htmlspecialchars($email)?>" /></td></tr>
				<tr><td><?=$s['OLD_PASSWD']?></td><td><input type="password" name="old" class="input-field" /></td></tr>
				<tr><td><?=$s['NEW_PASSWD']?></td><td><input type="password" name="new" class="input-field" /></td></tr>
				<tr><td><?=$s['REPEAT_PASSWD']?></td><td><input type="password" name="repeat" class="input-field" /></td></tr>
			</table>
			<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
		</form>