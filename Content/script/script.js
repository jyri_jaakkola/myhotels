$(document).ready(function () {
	$('.login-menu').hide();
	
	$('.login').click(function() {
		$('.login-menu').toggle();
	});
	
	$('.dashboard_part').hide();
	
	$('#new_hotel').click(function() {
		$('.dashboard_part').hide();
		$('#add_new_hotel').show();
	});
	
	$('#new_employee').click(function() {
		$('.dashboard_part').hide();
		$('#add_new_employee').show();
	});
	
	$('#new_reservation').click(function() {
		$('.dashboard_part').hide();
		$('#add_new_reservation').show();
	});
	
	$('#new_customer').click(function() {
		$('.dashboard_part').hide();
		$('#customer').show();
	});
	
	$rooms = $(".products").find(".button-blue");
	$($rooms).each(function() {
		$(this).click(function() {
			if ($(this).next('.dashboard_part').is(':visible')) {
				$(this).next('.dashboard_part').hide();
			} else {
				$('.dashboard_part').hide();
				$(this).next('.dashboard_part').show();
			}
		});
	});
	
	$services = $(".services").find(".button-blue");
	$($services).each(function() {
		$(this).click(function() {
			if ($(this).next('.dashboard_part').is(':visible')) {
				$(this).next('.dashboard_part').hide();
			} else {
				$('.dashboard_part').hide();
				$(this).next('.dashboard_part').show();
			}
		});
	});
});