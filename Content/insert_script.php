<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
<title></title>
</head>
<body>
<?php
	function APIgenerator(){
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
		$pass[0] = time();
        for ($i = 1; $i < 11; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $pass[$i] = $alphabet[$n];
        }
        $base = implode($pass);
		
		return md5($base);
    }
	
	function hashPassword($password){
        return password_hash($password, PASSWORD_BCRYPT);
    }

	$names = array(1 => array("etu" => "Johan", "suku" => "Johtaja"),
				   2 => array("etu" => "Make", "suku" => "Siivooja"),
				   3 => array("etu" => "Maiju", "suku" => "Siivooja"),
				   4 => array("etu" => "Mike", "suku" => "Markkinoija"),
				   5 => array("etu" => "Masa", "suku" => "Markkina"),
				   6 => array("etu" => "Tiina", "suku" => "Tuote"),
				   7 => array("etu" => "Taisto", "suku" => "Tähdellinen"),
				   8 => array("etu" => "Jaana", "suku" => "Jämäkkä"),
				   9 => array("etu" => "Ismo", "suku" => "Insinööri"),
				   10 => array("etu" => "Peke", "suku" => "Pomo"));

	echo "-- TYÖNTEKIJÄT --\n\n";
	foreach($names as $n) {
		echo "INSERT INTO tyontekija (etu, suku, email, palkka, syntymaaika, api, salasana) \nVALUES (";
		echo "'". $n['etu'] ."', '". $n['suku'] ."', '";
		$suku = str_replace('ä', 'a', $n['suku']);
		$suku = str_replace('ö', 'o', $suku);
		$email = strtolower($n['etu']) .".". strtolower($suku) ."@myhotels.fi";
		$palkka = floor(rand(1000, 7000)/100)*100;
		$synty =  rand(1950, 1990) . "-" . rand(1,12) . "-" . rand(1,28);
		echo $email ."', ". $palkka .", '" . $synty ."', '". APIgenerator() ."', '". hashPassword("Salasana4") ."');\n\n";
	}
	
	
	$names2 = array(1 => array("etu" => "Unelma", "suku" => "Säleikkö"),
				    2 => array("etu" => "Inkeri", "suku" => "Möttönen"),
				    3 => array("etu" => "Kaarina", "suku" => "Virtanen"),
				    4 => array("etu" => "Daniel", "suku" => "Järvinen"),
				    5 => array("etu" => "Viljami", "suku" => "Ketonen"),
				    6 => array("etu" => "Elmeri", "suku" => "Pesonen"),
				    7 => array("etu" => "Elmo", "suku" => "Matikka"),
				    8 => array("etu" => "Riina", "suku" => "Isomäki"),
				    9 => array("etu" => "Pertti", "suku" => "Perustelija"),
				    10 => array("etu" => "Tapio", "suku" => "Suominen"));
	
	echo "-- ASIAKKAAT --\n\n";	
	foreach($names2 as $n) {
		echo "INSERT INTO asiakas (etu, suku, email, syntymaaika, api, salasana) \nVALUES (";
		echo "'". $n['etu'] ."', '". $n['suku'] ."', '";
		$suku = str_replace('ä', 'a', $n['suku']);
		$suku = str_replace('ö', 'o', $suku);
		$email = strtolower($n['etu']) .".". strtolower($suku) ."@asiakas.fi";
		$synty =  rand(1965, 1997) . "-" . rand(1,12) . "-" . rand(1,28);
		echo $email ."', '". $synty ."', '". APIgenerator() ."', '". hashPassword("Salasana4") ."');\n\n";
	}
	
	echo "-- KETJU --\n\n";
	echo "INSERT INTO ketju (nimi, johtajaID) VALUES ('MyHotels', 1);\n\n";
	
	echo "-- TOIMIPISTEET --\n\n";
	echo "INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Staten Hotelli', 1, ". rand(1,10) .");\n\n";
	echo "INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Atomic Hotelli', 1, ". rand(1,10) .");\n\n";
	echo "INSERT INTO toimipiste (nimi, ketjuID, johtajaID) VALUES ('Fisher Hotelli', 1, ". rand(1,10) .");\n\n";
	
	echo "-- HUONEET --\n\n";
	for ($i = 1; $i < 4; $i++) {
		for ($j = 1; $j < 4; $j++) {
			echo "INSERT INTO huone (nimi, kuvaus, hinta, toimipisteID) VALUES ('Halppis ".$j."', 'Tämä huone on halpa.', 50, ". $i .");\n\n";
		}
		for ($j = 1; $j < 4; $j++) {
			echo "INSERT INTO huone (nimi, kuvaus, hinta, toimipisteID) VALUES ('Perus ".$j."', 'Tämä huone on keskihintainen.', 110, ". $i .");\n\n";
		}
		for ($j = 1; $j < 4; $j++) {
			echo "INSERT INTO huone (nimi, kuvaus, hinta, toimipisteID) VALUES ('Luxus ".$j."', 'Tämä huone on kallis.', 200, ". $i .");\n\n";
		}
	}
	
	
	echo "-- PALVELUT --\n\n";
	for ($i = 1; $i < 4; $i++) {
		echo "INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Ravintola', 'Ravintola tarjoaa ruokaa.', 20, ". $i .");\n\n";
		echo "INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kuntosali', 'Kuntosalilla voit treenata.', 5, ". $i .");\n\n";
		echo "INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Sauna', 'Saunassa on lämmin.', 5, ". $i .");\n\n";
		echo "INSERT INTO palvelu (nimi, kuvaus, hinta, toimipisteID) VALUES ('Kauneushoitola', 'Kauneushoitolassa kauneus kuntoon.', 30, ". $i .");\n\n";
	}
	
	echo "-- HUONEVARAUS --\n\n";
	for ($i = 1; $i < 20; $i++) {
		$kk = rand(1,12);
		$pp = rand(1,15);
		echo "INSERT INTO huoneVaraus (alku, loppu, asiakasID, huoneID) VALUES ('2014-".$kk."-".$pp."', '2014-".$kk."-".($pp+5)."', ".rand(1,10).", ".rand(1,27).");\n\n";
	}
	
	echo "-- PALVELUVARAUS --\n\n";
	for ($i = 1; $i < 20; $i++) {
		$kk = rand(1,12);
		$pp = rand(1,28);
		echo "INSERT INTO palveluVaraus (pvm, asiakasID, palveluID) VALUES ('2014-".$kk."-".$pp."', ".rand(1,10).", ".rand(1,12).");\n\n";
	}
?>
</body>
</html>