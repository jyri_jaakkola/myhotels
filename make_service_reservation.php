<?php
	$title = "MyHotels - Tee varaus";
	require("config.php");
	if(isset($_SESSION['id']) && $_SESSION['role'] == 1) {
		$action = NULL;
		if(isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		switch($action) {
			case 'confirm':
				$service->makeReservation($_POST['date'], $_POST['office'], $_POST['service'], $_SESSION['id']);
				header("Location: index.php");
				break;
			default:
				$data = $service->serviceInfo($_GET['office'], $_GET['service']);
				$user_data = $user->getUserData();
				require("Views/Reservation.confirm.view.php");
		}
		
	} else if ($_SESSION['role'] > 1) {
		$action = NULL;
		if (isset($_GET['action'])) {
			$action = $_GET['action'];
		}
		
		switch($action) {
			case 'confirm':
				if ($_POST['id'] == 0) {
					$id = NULL;
				} else {
					$id = $_POST['id'];
				}
				$service->makeReservation($_POST['date'], $_POST['office'], $_POST['service'], $id);
				header("Location: index.php");
				break;
			default:
				$data = $service->serviceInfo($_GET['office'], $_GET['service']);
				$customers = $user->getCustomers();
				require("Views/Reservation.confirm.employee.view.php");
		}
	} else {
		header("Location: index.php");
	}
?>