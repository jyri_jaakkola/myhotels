<?php
	include_once("Parts/Header.view.php");

	if ($user->isLogged() > 1) {
		$id = $officeInfo['toimipisteID'];
		$nimi = $officeInfo['nimi'];
		$nimi = utf8_encode($nimi);
		$ketju = $officeInfo['ketjuID'];
		$johtaja = $officeInfo['johtajaID'];
	
?>
	<h2><?=$s['DELETE_OFFICE']?></h2>

	<form method="POST" action="office_info.php?action=delete">
		<input type="hidden" name="id" value="<?=$id?>" />
		<input type="submit" class="button-red" value="<?=$s['DELETE']?>" />
	</form>

	<h2><?=$s['UPDATE_OFFICE']?></h2>
		
	<form method="POST" action="office_info.php?action=update">
		<table>
			<input type="hidden" name="id" value="<?=$id?>" />
			<tr><td><?=$s['OFFICE_NAME']?></td><td><input type="text" name="nimi" class="input-field" value="<?=htmlspecialchars($nimi)?>" /></td></tr>
			<?php if ($user->isLogged() == 4) { ?>
				<tr><td><?=$s['EMAIL']?></td><td>
					<select name="director" class="input-field">
						<?php
							foreach ($employees as $e => $name) {
								echo "<option value='".$e."'";
								if ($e == $johtaja) {
									echo " selected";
								}
								echo ">".utf8_encode($name)."</option>";
							}
						?>
					</select>
				</td></tr>
			
			<?php } ?>
		</table>
		<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
	</form>

	<div class="divider"></div>
	<h2><?=$s['OFFICE_CUSTOMERS_IN']?></h2>
<?php
	}
	
	echo "<table>\n";
	foreach($customers as $counter => $array) {
		echo "<tr>";
		foreach($array as $value) {
			echo "<td>". utf8_encode($value) ."</td>";
		}
		echo "</tr>\n";
	}
	echo "</table>\n";
?>
<?php
	include_once("Parts/Footer.view.php");
?>