<?php
	include_once("Parts/Header.view.php");
?>
	<div class="products">
	
	<h2><?=$s['ROOMS']?></h2>
<?php
	foreach ($rooms AS $id => $values) {
?>
		<div class="product-row">
			<h3><?=$values['nimi']?></h3> <button class="button-blue"><?=$s['EDIT']?></button>
		
			<div class="dashboard_part">
				<form method="POST" action="?action=update">
					<input type="hidden" name="tid" value="<?=$_POST['tid']?>" />
					<input type="hidden" name="id" value="<?=$id?>" />
					<table>
						<tr><td><?=$s['ROOM_TYPE']?></td><td><input type="text" class="input-field" name="type" value="<?=utf8_encode($values['tyyppi'])?>" /></td></tr>
						<tr><td><?=$s['ROOM_NAME']?></td><td><input type="text" class="input-field" name="name" value="<?=utf8_encode($values['nimi'])?>" /></td></tr>
						<tr><td><?=$s['DESCRIPTION']?></td><td><input type="text" class="input-field" name="desc" value="<?=utf8_encode($values['kuvaus'])?>" /></td></tr>
						<tr><td><?=$s['PRICE']?></td><td><input type="text" class="input-field" name="price" value="<?=utf8_encode($values['hinta'])?>" /></td></tr>
					</table>
					<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
				</form>
				<form method="POST" action="?action=delete">
					<input type="hidden" name="tid" value="<?=$_POST['tid']?>" />
					<input type="hidden" name="id" value="<?=$id?>" />
					<input type="submit" class="button-red" value="<?=$s['DELETE']?>" />
				</form>
			</div>
			<div class="clearboth"></div>
		</div>
<?php	
	}
?>
	</div>
	
	<div class="services">
	<h2><?=$s['SERVICES']?></h2>
	
<?php
	foreach ($services AS $id => $values) {
?>
		<div class="product-row">
			<h3><?=$values['nimi']?></h3> <button class="button-blue"><?=$s['EDIT']?></button>
		
			<div class="dashboard_part">
				<form method="POST" action="?action=updates">
					<input type="hidden" name="tid" value="<?=$_POST['tid']?>" />
					<input type="hidden" name="id" value="<?=$id?>" />
					<table>
						<tr><td><?=$s['SERVICE_NAME']?></td><td><input type="text" class="input-field" name="name" value="<?=utf8_encode($values['nimi'])?>" /></td></tr>
						<tr><td><?=$s['DESCRIPTION']?></td><td><input type="text" class="input-field" name="desc" value="<?=utf8_encode($values['kuvaus'])?>" /></td></tr>
						<tr><td><?=$s['PRICE']?></td><td><input type="text" class="input-field" name="price" value="<?=utf8_encode($values['hinta'])?>" /></td></tr>
					</table>
					<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
				</form>
				<form method="POST" action="?action=deletes">
					<input type="hidden" name="tid" value="<?=$_POST['tid']?>" />
					<input type="hidden" name="id" value="<?=$id?>" />
					<input type="submit" class="button-red" value="<?=$s['DELETE']?>" />
				</form>
			</div>
			<div class="clearboth"></div>
		</div>
<?php	
	}
?>
	
	</div>
<?php
	include_once("Parts/Footer.view.php");
?>