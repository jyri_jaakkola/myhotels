<?php
	include_once("Parts/Header.view.php");
?>

	<h1><?=$s['DASHBOARD']?></h1>
	
	<div class="dashboard-navi">
		<ul>
			<?php if($user->isLogged() >= 3) { ?>
				<li id="new_hotel"><img src="Content/images/new_hotel.png" alt="hotel" /><div><?=$s['HOTEL']?></div></li>
				<li id="new_employee"><img src="Content/images/new_employee.png" alt="employee" /><div><?=$s['EMPLOYEE']?></div></li>
			<?php } ?>
			<li id="new_reservation"><img src="Content/images/new_reservation.png" alt="reservation" /><div><?=$s['PRODUCTS']?></div></li>
			<li id="new_customer"><img src="Content/images/new_user.png" alt="customer" /><div><?=$s['CONTAIN_USERS']?></div></li>
		</ul>
	</div>
	
	<?php
		// Ketjun johtaja ja toimipisteen johtaja
		if ($user->isLogged() >= 3) {
	?>
		<div class= "dashboard_part" id="add_new_hotel">
			
			<h2><?=$s['SEARCH_HOTEL']?></h2>
			
			<?php include("Dashboard/SearchHotel.view.php"); ?>
			
			<?php if ($user->isLogged() == 4) { ?>
				<div class="divider"></div>
			
				<h2><?=$s['NEW_HOTEL']?></h2>
				
				<?php include("Dashboard/AddHotel.view.php"); ?>
			
			<?php } ?>
		</div>

		<div class= "dashboard_part" id="add_new_employee">
			<h2><?=$s['SEARCH_EMPLOYEE']?></h2>
		
			<?php include ("Dashboard/SearchEmployee.view.php"); ?>
			
			<div class="divider"></div>
			
			<h2><?=$s['NEW_EMPLOYEE']?></h2>
			
			<?php include("Dashboard/AddEmployee.view.php"); ?>
		</div>
	<?php
		}
		
		// Normaali työntekijä
	?>
	
	<div class= "dashboard_part" id="add_new_reservation">
		
		<h2><?=$s['OFFICE_PRODUCTS']?></h2>
		
		<?php include ("Dashboard/SearchHotelProducts.view.php"); ?>
	
		<div class="divider"></div>
		
		<h2><?=$s['NEW_ROOM']?></h2>
		
		<?php include("Dashboard/AddRoom.view.php"); ?>
		
		<div class="divider"></div>
		
		<h2><?=$s['NEW_SERVICE']?></h2>
		
		<?php include("Dashboard/AddService.view.php"); ?>
	</div>
	
	<div class= "dashboard_part" id="customer">
		<h2><?=$s['SEARCH_USER']?></h2>
		
		<?php include ("Dashboard/SearchCustomer.view.php"); ?>
		
		<div class="divider"></div>
	
		<h2><?=$s['CREATE_USER']?></h2>
	
		<?php include("Dashboard/AddCustomer.view.php"); ?>
	</div>

<?php
	include_once("Parts/Footer.view.php");
?>