<?php
	include_once("./Views/Parts/Header.view.php");
	
	$year = NULL;
	$month = NULL;
	
	if(isset($_POST['year'])) {
		$year = $_POST['year'];
		$month = $_POST['month'];
	}
?>
	
	<h1><?=$s['MONTHLY_REPORT']?></h1>
	
	<form method="POST" action="?action=get">
		<p><?=$s['YEAR']?> <select name="year">
			<?php
				$range = range(2012,date('Y'));
				$range = array_reverse($range);
				foreach($range as $y) {
					echo "<option value=". $y;
					if ($y == $year) {
						echo " selected";
					}
					echo ">". $y ."</option>\n";
				}
			?>
		</select>
		<?=$s['MONTH']?> <select name="month">
			<?php
				$range = range(1,12);
				foreach($range as $m) {
					echo "<option value=". $m;
					if ($m == $month) {
						echo " selected";
					}
					echo ">". $m ."</option>\n";
				}
			?>
		</select>
		<input type="submit" class="button" value="<?=$s['GET']?>"/>
		</p>
	</form>
	
<?php
	echo "<table class='report'>\n";
	echo "<tr><th>". $s['HOTEL'] ."</th><th>". $s['SERVICE_SALES'] ."</th><th>". $s['ROOM_SALES'] ."</th></tr>\n";
	if (isset($_POST['year'])) {
		foreach ($data as $k => $val) {
			$reports->printLine($val);
		}
	}
	echo "</table>\n";

	
	include_once("./Views/Parts/Footer.view.php");
?>