<?php
	include_once("Parts/Header.view.php");
	$file = explode("/", $_SERVER['PHP_SELF']);
	$file = $file[count($file)-1];
?>

<form method="POST" action="?action=confirm">
	<table>
		<tr><td><?=$s['CUSTOMER']?></td><td>
			<select name="id" class="input-field">
				<?php
					if (preg_match("/^make_service/", $file)) {
						echo "<option value='0'>Default</option>\n";
					}
					foreach ($customers as $id => $name) {
						echo "<option value='".$id."'>".htmlspecialchars(utf8_encode($name))."</option>\n";
					}
				?>
			</select>
			</td></tr>

<?php
	if(preg_match("/^make_service/", $file)) {
?>

		<tr><td><?=$s['PRICE']?></td><td><?=$data['hinta']?> &euro;</td></tr>
		<tr><td><?=$s['SERVICE']?></td><td><?=$data['nimi']?></td></tr>
		<input type="hidden" name="office" value="<?=$_GET['office']?>" />
		<input type="hidden" name="date" value="<?=$_GET['date']?>" />
		<input type="hidden" name="service" value="<?=$_GET['service']?>" />
<?php
	} if (preg_match("/^make_reservation/", $file)) {
?>
		<input type="hidden" name="office" value="<?=$_GET['office']?>" />
		<input type="hidden" name="start" value="<?=$_GET['start']?>" />
		<input type="hidden" name="end" value="<?=$_GET['end']?>" />
		<input type="hidden" name="room" value="<?=$_GET['room']?>" />
		
		<tr><td><?=$s['ROOM']?></td><td><?=$data['tyyppi']?></td></tr>
		<tr><td><?=$s['TIME']?></td><td><?=$_GET['start']?> - <?=$_GET['end']?></td></tr>
		<tr><td><?=$s['TOTAL_PRICE']?></td><td>
<?php
		$datetime1 = new DateTime($_GET['start']);
		$datetime2 = new DateTime($_GET['end']);
		$interval = $datetime1->diff($datetime2);
		$diff = $interval->format('%a');

		echo $data['hinta'] * $diff . " &euro;</td></tr>\n";
	
	}
?>

	</table>
	<input type="submit" class="button" value="<?=$s['CONFIRM']?>" />
</form>

<?php
	include_once("Parts/Footer.view.php");
?>