<?php
	if (isset($_POST['first'])) {
		$first = htmlspecialchars($_POST['first']);
		$last = htmlspecialchars($_POST['last']);
	} else {
		$first = "";
		$last = "";
	}
?>

<form method="POST" action="dashboard.php?action=new_customer">
	<table>
		<tr><td><?php echo $s['FIRST_NAME'] ?></td><td><input type="text" class="input-field" name="first" value="<?php echo $first ?>" /></td></tr>
		<tr><td><?php echo $s['LAST_NAME'] ?></td><td><input type="text" class="input-field" name="last" value="<?php echo $last ?>" /></td></tr>

		<tr><td><?php echo $s['CUSTOMER'] ?></td><td>
			<select name="customer" class="input-field">
				<?php
					foreach ($customers as $id => $name) {
						echo "<option value='".$id."'>".utf8_encode($name)."</option>\n";
					}
				?>
			</select>
		</td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['CREATE'] ?>" />
</form>