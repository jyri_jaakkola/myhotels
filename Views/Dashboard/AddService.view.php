<?php
	if (isset($_POST['office-id-service'])) {
		$office = htmlspecialchars($_POST['office-id-service']);
		$type = htmlspecialchars($_POST['type']);
		$service_name = htmlspecialchars($_POST['service_name']);
		$service_desc = htmlspecialchars($_POST['service_desc']);
		$service_price = htmlspecialchars($_POST['service_price']);
	} else {
		$office = "";
		$type = "";
		$service_name = "";
		$service_desc = "";
		$service_price = "";
	}
?>

<form method="POST" action="dashboard.php?action=new_service">
	<table>
		<tr><td><?php echo $s['HOTEL'] ?></td><td>
			<select name="office-id-service" class="input-field">
				<?php
					foreach ($offices as $id => $array) {
						if ($user->isLogged() == 4 || in_array($array['toimipisteID'], $_SESSION['office']) || $array['toimipisteID'] == $user->getOffice()) {
							echo "<option value='".$array['toimipisteID']."'>".htmlspecialchars(utf8_encode($array['nimi']))."</option>\n";
						}
					}
				?>
			</select></td></tr>
		<tr><td><?=$s['SERVICE_NAME']?></td><td><input type="text" class="input-field" name="service_name" value="<?=$service_name?>" /></td></tr>
		<tr><td><?=$s['DESCRIPTION']?></td><td><input type="text" class="input-field" name="service_desc" value="<?=$service_desc?>" /></td></tr>
		<tr><td><?=$s['PRICE']?></td><td><input type="text" class="input-field" name="service_price" value="<?=$service_price?>" /></td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['CREATE'] ?>" />
</form>