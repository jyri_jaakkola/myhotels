<?php
	if (isset($_POST['office-id'])) {
		$office = htmlspecialchars($_POST['office-id']);
		$type = htmlspecialchars($_POST['type']);
		$room_name = htmlspecialchars($_POST['room_name']);
		$room_desc = htmlspecialchars($_POST['room_desc']);
		$room_price = htmlspecialchars($_POST['room_price']);
	} else {
		$office = "";
		$type = "";
		$room_name = "";
		$room_desc = "";
		$room_price = "";
	}
?>

<form method="POST" action="dashboard.php?action=new_room">
	<table>
		<tr><td><?php echo $s['HOTEL'] ?></td><td>
			<select name="office-id" class="input-field">
				<?php
					foreach ($offices as $id => $array) {
						if ($user->isLogged() == 4 || in_array($array['toimipisteID'], $_SESSION['office']) || $array['toimipisteID'] == $user->getOffice()) {
							echo "<option value='".$array['toimipisteID']."'>".htmlspecialchars(utf8_encode($array['nimi']))."</option>\n";
						}
					}
				?>
			</select></td></tr>
		<tr><td><?=$s['ROOM_TYPE']?></td><td><input type="text" class="input-field" name="type" value="<?=$type?>" /></td></tr>
		<tr><td><?=$s['ROOM_NAME']?></td><td><input type="text" class="input-field" name="room_name" value="<?=$room_name?>" /></td></tr>
		<tr><td><?=$s['DESCRIPTION']?></td><td><input type="text" class="input-field" name="room_desc" value="<?=$room_desc?>" /></td></tr>
		<tr><td><?=$s['PRICE']?></td><td><input type="text" class="input-field" name="room_price" value="<?=$room_price?>" /></td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['CREATE'] ?>" />
</form>