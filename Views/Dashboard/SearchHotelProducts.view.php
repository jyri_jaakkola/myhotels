<form method="POST" action="office_products.php">
	<table>
		<tr><td><?php echo $s['HOTEL'] ?></td><td>
			<select name="tid" class="input-field">
				<?php
					foreach ($offices as $id => $array) {
						if ($user->isLogged() == 4 || in_array($array['toimipisteID'], $_SESSION['office']) || $array['toimipisteID'] == $user->getOffice()) {
							echo "<option value='".$array['toimipisteID']."'>".htmlspecialchars(utf8_encode($array['nimi']))."</option>\n";
						}
					}
				?>
			</select></td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['SEARCH_INFO'] ?>" />
</form>