<?php
	if (isset($_POST['office'])) {
		$office = htmlspecialchars($_POST['office']);
		$manager = htmlspecialchars($_POST['manager']);
	} else {
		$office = "";
		$manager = "";
	}
?>

<form method="POST" action="dashboard.php?action=new_office">
	<table>
		<tr><td><?php echo $s['OFFICE_NAME'] ?></td><td><input type="text" class="input-field" name="office" value="<?php echo $office ?>" /></td></tr>
		<tr><td><?php echo $s['MANAGER'] ?></td><td>
			<select name="manager" class="input-field">
				<?php
					foreach ($employees as $id => $name) {
						echo "<option value='".$id."'>".utf8_encode($name)."</option>\n";
					}
				?>
			</select>
		</td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['CREATE'] ?>" />
</form>