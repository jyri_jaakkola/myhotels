<?php
	if (isset($_POST['first'])) {
		$first = htmlspecialchars($_POST['first']);
		$last = htmlspecialchars($_POST['last']);
		$email = htmlspecialchars($_POST['email']);
		$birth = htmlspecialchars($_POST['birth']);
	} else {
		$first = "";
		$last = "";
		$email = "";
		$birth = "";
	}
?>

<form method="POST" action="dashboard.php?action=new_customer">
	<table>
		<tr><td><?php echo $s['FIRST_NAME'] ?></td><td><input type="text" class="input-field" name="first" value="<?php echo $first ?>" /></td></tr>
		<tr><td><?php echo $s['LAST_NAME'] ?></td><td><input type="text" class="input-field" name="last" value="<?php echo $last ?>" /></td></tr>

		<tr><td><?php echo $s['EMAIL'] ?></td><td><input type="text" class="input-field" name="email" value="<?php echo $email ?>" /></td></tr>
		<tr><td><?php echo $s['BIRTHDAY'] ?></td><td><input type="text" class="input-field" name="birth" value="<?php echo $birth ?>" /></td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['CREATE'] ?>" />
</form>