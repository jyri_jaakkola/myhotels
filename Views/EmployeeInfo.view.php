<?php
	include_once("Parts/Header.view.php");
	
	if ($user->isLogged() > 1) {
		$id = $data['id'];
		$etu = $data['etu'];
		$etu = utf8_encode($etu);
		$suku = $data['suku'];
		$suku = utf8_encode($suku);
		$email = $data['email'];
		$office = $data['toimipisteID'];
		$salary = $data['palkka'];
	
?>
	<h2><?=$s['DELETE_EMPLOYEE']?></h2>

	<form method="POST" action="delete_user.php?action=employee">
		<input type="hidden" name="id" value="<?=$id?>" />
		<input type="submit" class="button-red" value="<?=$s['DELETE']?>" />
	</form>

	<h2><?=$s['UPDATE_EMPLOYEE']?></h2>
		
	<form method="POST" action="employee_info.php?action=update">
		<table>
			<input type="hidden" name="id" value="<?=$id?>" />
			<tr><td><?=$s['FIRST_NAME']?></td><td><input type="text" name="etu" class="input-field" value="<?=htmlspecialchars($etu)?>" /></td></tr>
			<tr><td><?=$s['LAST_NAME']?></td><td><input type="text" name="suku" class="input-field" value="<?=htmlspecialchars($suku)?>" /></td></tr>
			<tr><td><?=$s['EMAIL']?></td><td><input type="text" name="email" class="input-field" value="<?=htmlspecialchars($email)?>" /></td></tr>
			<tr><td><?=$s['SALARY']?></td><td><input type="text" name="salary" class="input-field" value="<?=htmlspecialchars($salary)?>" /></td></tr>
			<tr><td><?=$s['OFFICE']?></td>
				<td><select name="office" class="input-field">
				<?php
					$offices = $room->hotelList();
					foreach ($offices as $o) {
						echo "<option value='".$o['toimipisteID']."'";
						if ($o['toimipisteID'] == $office) {
							echo " selected";
						}
						echo ">".$o['nimi']."</option>\n";
					}
				?>
			</select></td>
			</tr>
		</table>
		<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
	</form>

<?php
	}
?>
<?php
	include_once("Parts/Footer.view.php");
?>