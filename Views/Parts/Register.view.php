<?php
	if (isset($_POST['first'])) {
		$first = htmlspecialchars($_POST['first']);
		$last = htmlspecialchars($_POST['last']);
		$email = htmlspecialchars($_POST['email']);
		$birth = htmlspecialchars($_POST['birth']);
	} else {
		$first = "";
		$last = "";
		$email = "";
		$birth = "";
	}
?>

<h1><?php echo $s['REGISTER'] ?></h1>

<form method="POST" action="register.php?action=register">
	<table>
		<tr><td><?php echo $s['FIRST_NAME'] ?></td><td><input type="text" name="first" value="<?php echo $first ?>" /></td></tr>
		<tr><td><?php echo $s['LAST_NAME'] ?></td><td><input type="text" name="last" value="<?php echo $last ?>" /></td></tr>

		<tr><td><?php echo $s['EMAIL'] ?></td><td><input type="text" name="email" value="<?php echo $email ?>" /></td></tr>
		<tr><td><?php echo $s['BIRTHDAY'] ?></td><td><input type="text" name="birth" value="<?php echo $birth ?>" /></td></tr>
		
		<tr><td><?php echo $s['PASSWD'] ?></td><td><input type="password" name="pass" /></td></tr>
		<tr><td><?php echo $s['REPEAT_PASSWD'] ?></td><td><input type="password" name="repeat" /></td></tr>
	</table>
	<input type="submit" name="nappi" class="button" value="<?php echo $s['REGISTER'] ?>" />
</form>