<?php
	if(isset($_SESSION['notification'])) {
		if($_SESSION['success'] == true) {
?>

<script>
	noty({ 
		text: '<?=$_SESSION['notification']?>',
		type: 'success',
		timeout: 3000
	});
</script>

<?php
		} else {
?>

<script>
	noty({
		text: '<?=$_SESSION['notification']?>',
		type: 'error',
		timeout: 5000
	});
</script>	
	
<?php		
		}
		unset($_SESSION['notification']);
		unset($_SESSION['success']);
	}
?>