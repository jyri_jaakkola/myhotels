<script>
	$(function() {
	$( "#from" ).datepicker({
		changeMonth: true,
		dateFormat: "yy-mm-dd",
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate);
		}
	});
	$( "#to" ).datepicker({
		changeMonth: true,
		numberOfMonths: 1,
		dateFormat: "yy-mm-dd",
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	});
</script>

<?php
	$hotel=1;
	$from=null;
	$to=null;
	if(isset($_POST['hotel'])) {
		$hotel=$_POST['hotel'];
	}
	if(isset($_POST['from'])) {
		$from = $_POST['from'];
	}
	if(isset($_POST['to'])) {
		$to = $_POST['to'];
	}
?>

<div class="datepicker">
	<form method="POST" action="?action=date">
		<h2><?=$s['SEARCH_ROOM']?></h2>

		<div class="date-label"><?=$s['HOTEL']?></div>
		<select name="hotel" class="date-value">
			<?php
				$hotels = $room->hotelList();
				foreach ($hotels as $key => $h) {
					echo "<option value=\"". $h['toimipisteID'] . "\"";
					if ($h['toimipisteID'] == $hotel) {
						echo " selected";
					}
					echo ">". $h['nimi'] ."</option>\n";
				}
			?>
		</select>

		<div class="date-label">From</div>
		<input type="text" class="date-value" id="from" name="from" autocomplete="off" value="<?=$from?>" />

		<div class="date-label">to</div>
		<input type="text" class="date-value" id="to" name="to" autocomplete="off" value="<?=$to?>" />

		<input type="submit" class="button" value="Hae" />
	</form>
</div>