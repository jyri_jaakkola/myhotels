<script>
	$(function() {
	$( "#from" ).datepicker({
		changeMonth: true,
		dateFormat: "yy-mm-dd"
	});
	});
</script>

<?php
	$hotel=1;
	$date=null;
	$to=null;
	if(isset($_POST['hotel'])) {
		$hotel=$_POST['hotel'];
	}
	if(isset($_POST['date'])) {
		$date = $_POST['date'];
	}
?>

<div class="datepicker">
	<form method="POST" action="?action=date">
		<h2><?=$s['SEARCH_ROOM']?></h2>

		<div class="date-label"><?=$s['HOTEL']?></div>
		<select name="hotel" class="date-value">
			<?php
				$hotels = $room->hotelList();
				foreach ($hotels as $key => $h) {
					echo "<option value=\"". $h['toimipisteID'] . "\"";
					if ($h['toimipisteID'] == $hotel) {
						echo " selected";
					}
					echo ">". $h['nimi'] ."</option>\n";
				}
			?>
		</select>

		<div class="date-label">Date</div>
		<input type="text" class="date-value" id="from" name="date" autocomplete="off" value="<?=$date?>" />

		<input type="submit" class="button" value="Hae" />
	</form>
</div>