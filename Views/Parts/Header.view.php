<!DOCTYPE html>
<html lang="fi">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf8" />
    <title><?php echo $title; ?></title>
	<link rel="stylesheet" href="Content/css/reset.css" type="text/css" />
	<link rel="stylesheet" href="Content/css/tyylit.css" type="text/css" />
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script type='text/javascript' src='Content/script/script.js'></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script type="text/javascript" src="Content/script/js/noty/packaged/jquery.noty.packaged.min.js"></script>
	
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>
<body>
	<?php
		include("Views/Parts/Notifications.view.php");
	?>

	<div class="wrapper">
		<div class="header">
			
				<img src="Content/images/logo.png" alt="logo" class="logo-img" />
				
				<?php
					include("Views/Parts/Navi.view.php");
				?>
			
			<!--<a href="?en-EN"><img src="Content/images/GB.png" alt="en-EN" /></a>
			<a href="?fi-FI"><img src="Content/images/FI.png" alt="fi-FI" /></a>-->
		</div>
	
		<div class="content">