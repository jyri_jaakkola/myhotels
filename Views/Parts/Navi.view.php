<ul class="navi">
	<li><a href="./"><?=$s['HOME']?></a></li>
	<li><a href="roomlist.php"><?=$s['SEARCH_ROOM']?></a></li>
	<li><a href="servicelist.php"><?=$s['SEARCH_SERVICE']?></a></li>
	<li class="login"><img src="Content/images/<?php if($user->isLogged()) { echo "settings.png"; } else { echo "lock.png"; } ?>" alt="<?php echo $s['LOGIN']; ?>" /></li>
</ul>


<div class="login-menu">
	<?php 
		if ($user->isLogged()) {
			include("Views/Parts/Usermenu.view.php");
		} else {
			include("Views/Parts/Login.view.php");
		}
	?>
</div>