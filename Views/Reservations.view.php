<?php
	include_once("Parts/Header.view.php");
	
	if ($user->isLogged() > 1) {
		$etu = $data['etu'];
		$etu = utf8_encode($etu);
		$suku = $data['suku'];
		$suku = utf8_encode($suku);
		$email = $data['email'];
	
?>
	<h2><?=$s['DELETE_CUSTOMER']?></h2>

	<form method="POST" action="delete_user.php?action=customer">
		<input type="hidden" name="id" value="<?=$id?>" />
		<input type="submit" class="button-red" value="<?=$s['DELETE']?>" />
	</form>

	<h2><?=$s['UPDATE_CUSTOMER']?></h2>
		
	<form method="POST" action="user_info.php?action=update">
		<table>
			<input type="hidden" name="id" value="<?=$id?>" />
			<tr><td><?=$s['FIRST_NAME']?></td><td><input type="text" name="etu" class="input-field" value="<?=htmlspecialchars($etu)?>" /></td></tr>
			<tr><td><?=$s['LAST_NAME']?></td><td><input type="text" name="suku" class="input-field" value="<?=htmlspecialchars($suku)?>" /></td></tr>
			<tr><td><?=$s['EMAIL']?></td><td><input type="text" name="email" class="input-field" value="<?=htmlspecialchars($email)?>" /></td></tr>
		</table>
		<input type="submit" class="button" value="<?=$s['UPDATE']?>" />
	</form>

	<h1><?php if ($user->isLogged() > 1) { echo $s['RESERVATIONS']; } else { echo $s['MY_RESERVETIONS']; } ?></h1>

<?php
	}
?>

	<h2>Huonevaraukset</h2>

	<table class='report'>
		<tr><th>Huoneen tyyppi</th><th>á hinta</th><th>Aloitus päivämäärä</th><th>Lopetus päivämäärä</th></tr>
<?php
	foreach($reservations as $k => $v) {
		echo "<tr>";
		foreach($v as $r) {
			echo "<td>". $r ."</td>";
		}
		echo "</tr>\n";
	}
?>
	</table>
	
	<h2>Palveluvaraukset</h2>
	
	<table class='report'>
		<tr><th>Palvelu</th><th>Hinta</th><th>Päivämäärä</th></tr>
<?php
	foreach($services as $k => $v) {
		echo "<tr>";
		foreach($v as $r) {
			echo "<td>". $r ."</td>";
		}
		echo "</tr>\n";
	}
?>
	</table>

<?php
	include_once("Parts/Footer.view.php");
?>