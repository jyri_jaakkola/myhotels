# README #

MyHotels-project uses mostly PHP with some help from JavaScript and jQuery.

MyHotels was done as a school project for Databases-course in 2015.


# Set-up #

If you want to set this project up and test it here are the steps you need to follow:
    1. Copy files to PHP server.
    2. Set up database (0000_INFO/SQL/MyHotels_vr3.00.sql) and add data (0000_INFO/SQL/dataa.sql)
    3. Change database user information in /config.php and /api/API.class.php
And that should be all.

You may also just see project working on http://frozenfox.dy.fi/MyHotels/, if my server is running