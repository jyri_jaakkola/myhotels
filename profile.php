<?php
	$title = "MyHotels - Profiili";
	require("config.php");
	if(isset($_SESSION['id'])) {
	
		if(isset($_GET['action'])) {
			$action = $_GET['action'];
		} else {
			$action = NULL;
		}
	
		switch($action) {
			case 'update':
				if ($_POST['new'] == $_POST['repeat']) {
					$user->doUpdate($_POST['etu'], $_POST['suku'], $_POST['email'], $_POST['old'], $_POST['new']);
				} else {
					$_SESSION['notification'] = $error[6];
					$_SESSION['success'] = false;
				}
				$data = $user->getUserData();
				require("Views/Profile.view.php");
				break;
			default:
				$data = $user->getUserData();
				require("Views/Profile.view.php");
		}
		
	} else {
		header("Location: index.php");
	}
?>